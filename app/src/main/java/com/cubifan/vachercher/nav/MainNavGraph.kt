package com.cubifan.vachercher.nav

import android.content.SharedPreferences
import androidx.compose.runtime.Composable
import androidx.compose.runtime.rememberCoroutineScope
import androidx.navigation.NavHostController
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.navArgument
import com.cubifan.vachercher.MainViewModel
import com.cubifan.vachercher.NavPath
import com.cubifan.vachercher.Screen
import com.cubifan.vachercher.core.data.PastGame
import com.cubifan.vachercher.core.data.Player
import com.cubifan.vachercher.core.di.DependencyInjector
import com.cubifan.vachercher.feature.badge.BadgesView
import com.cubifan.vachercher.feature.camera.CameraView
import com.cubifan.vachercher.feature.dashboard.DashboardView
import com.cubifan.vachercher.feature.home.HomeView
import com.cubifan.vachercher.feature.leaderboard.WorldLeaderboard
import com.cubifan.vachercher.feature.login.LoginView
import com.cubifan.vachercher.feature.onboarding.OnboardingView
import com.cubifan.vachercher.feature.profile.ProfileView
import com.cubifan.vachercher.feature.registration.RegistrationView
import com.cubifan.vachercher.feature.room.RoomCreationView
import com.cubifan.vachercher.feature.room.RoomReconnectionView
import com.cubifan.vachercher.feature.room.RoomsView
import com.cubifan.vachercher.feature.theme_game.ThemeGameView
import com.cubifan.vachercher.feature.waiting.WaitingView
import com.cubifan.vachercher.utils.LocaleUtils
import com.cubifan.vachercher.utils.UserInfos
import com.google.firebase.auth.FirebaseAuth
import kotlinx.coroutines.launch

@Composable
fun MainNavGraph(
    navController: NavHostController,
    autoLoginSharedPrefs: SharedPreferences,
    onboardingSharedPrefs: SharedPreferences,
    firebaseTokenIdSharedPrefs: SharedPreferences,
    updatePastGames: () -> Unit,
    updateUser: () -> Unit,
    updateLeaderboard: () -> Unit,
    showSnackbar: () -> Unit,
    quitGame: (NavHostController) -> Unit,
    firebaseAuth: FirebaseAuth,
    pastGames: List<PastGame>,
    leaderboardPlayers: List<Player>,
    activityMainViewModel: MainViewModel,
) {
    NavHost(
        navController = navController,
        startDestination =
        if (autoLoginSharedPrefs.getBoolean(
                LocaleUtils.AUTOLOGIN,
                false
            ) && onboardingSharedPrefs.getBoolean(
                LocaleUtils.ONBOARDING_COMPLETED, false
            )
        ) {
            updatePastGames()
            updateUser()
            activityMainViewModel.assertFirebaseTokenId()
            Screen.Home.route
        } else if (autoLoginSharedPrefs.getBoolean(
                LocaleUtils.AUTOLOGIN,
                false
            ) && !onboardingSharedPrefs.getBoolean(
                LocaleUtils.ONBOARDING_COMPLETED, false
            )
        ) Screen.Onboarding.route else Screen.Login.route
    ) {
        composable(Screen.Login.route) {
            LoginView(
                login = {
                    autoLoginSharedPrefs.edit().putBoolean(LocaleUtils.AUTOLOGIN, true).apply()

                    if (onboardingSharedPrefs.getBoolean(LocaleUtils.ONBOARDING_COMPLETED, false)) {
                        updateUser()
                        updatePastGames()
                        navController.navigate(Screen.Home.route) { popUpTo(0) }
                    } else {
                        navController.navigate(Screen.Onboarding.route) { popUpTo(0) }
                    }
                },
                register = {
                    navController.navigate(Screen.Register.route)
                },
                mainViewModel = activityMainViewModel
            )
        }
        composable(Screen.Register.route) {
            RegistrationView(
                navUserCreated = {
                    showSnackbar()
                    navController.navigate(Screen.Login.route) { popUpTo(0) }
                })
        }
        composable(Screen.Onboarding.route) {
            OnboardingView(
                goToHomePage = {
                    onboardingSharedPrefs.edit().putBoolean(LocaleUtils.ONBOARDING_COMPLETED, true)
                        .apply()
                    autoLoginSharedPrefs.edit().putBoolean(LocaleUtils.AUTOLOGIN, true).apply()

                    updateUser()
                    updatePastGames()
                    navController.navigate(Screen.Home.route) { popUpTo(0) }
                }
            )
        }
        composable(Screen.Home.route) { backStack ->
            HomeView(
                pastGames = pastGames,
                mainSnackbarHostState = activityMainViewModel.snackbarHostState.value,
                goToThemeSelection = {
                    with(navController) {
                        navigate(Screen.ThemeGame.route)
                    }
                },
                showLeaderboard = {
                    updateLeaderboard()
                    navController.navigate(Screen.Leaderboard.route)
                },
                showProfile = { navController.navigate(Screen.Profile.route) },
                showBadges = { navController.navigate(Screen.Badges.route) },
            )
        }
        composable(Screen.Profile.route) {
            val scope = rememberCoroutineScope()

            ProfileView(
                logout = {
                    firebaseTokenIdSharedPrefs.edit().remove(LocaleUtils.FIREBASE_TOKEN).apply()
                    autoLoginSharedPrefs.edit().putBoolean(LocaleUtils.AUTOLOGIN, false).apply()
                    firebaseAuth.signOut()
                    navController.navigate(Screen.Login.route) { popUpTo(0) }
                },
                deleteUser = {
                    firebaseAuth.currentUser?.delete()?.addOnCompleteListener { task ->
                        if (task.isSuccessful) {
                            scope.launch {
                                DependencyInjector.API_SERVICE.delete(UserInfos.uid)
                            }
                            firebaseTokenIdSharedPrefs.edit().remove(LocaleUtils.FIREBASE_TOKEN)
                                .apply()
                            autoLoginSharedPrefs.edit().putBoolean(LocaleUtils.AUTOLOGIN, false)
                                .apply()
                            navController.navigate(Screen.Login.route) { popUpTo(0) }
                        }
                    }
                }
            )
        }
        composable(Screen.Badges.route) {
            BadgesView()
        }
        composable(route = Screen.Leaderboard.route) {
            WorldLeaderboard(leaderboardPlayers)
        }
        composable(Screen.ThemeGame.route) { backStack ->
            ThemeGameView(
                mainViewModel = activityMainViewModel,
                goToRoomCreation = { theme: Int ->
                    with(navController) {
                        navController.navigate(Screen.RoomGame.route + NavPath.Theme + NavPath.GameMode)
                        currentBackStackEntry?.arguments?.putBoolean(
                            NavPath.GameMode.argument,
                            false
                        )
                        currentBackStackEntry?.arguments?.putString(
                            NavPath.Theme.argument,
                            theme.toString()
                        )
                    }
                }
            )
        }
        composable(
            route = Screen.RoomGame.route + NavPath.Theme + NavPath.GameMode,
            arguments = listOf(
                navArgument(NavPath.Theme.argument) { nullable = true; type = NavType.StringType },
                navArgument(NavPath.GameMode.argument) { defaultValue = true }
            )
        ) { backStack ->
            RoomsView(
                backStack.arguments?.getString(NavPath.Theme.argument),
                enterRoom = { roomName ->
                    with(navController) {
                        navigate(Screen.Waiting.route + NavPath.Theme + NavPath.GameMode + NavPath.IsAdmin + NavPath.RoomName)
                        currentBackStackEntry?.arguments?.putBoolean(
                            NavPath.GameMode.argument,
                            backStack.arguments?.getBoolean(NavPath.GameMode.argument) == true
                        )
                        currentBackStackEntry?.arguments?.putString(
                            NavPath.Theme.argument,
                            backStack.arguments?.getString(NavPath.Theme.argument)
                        )
                        currentBackStackEntry?.arguments?.putBoolean(
                            NavPath.IsAdmin.argument,
                            false
                        )
                        currentBackStackEntry?.arguments?.putString(
                            NavPath.RoomName.argument,
                            roomName
                        )
                    }
                },
                createRoom = {
                    with(navController) {
                        navigate(Screen.CreateRoomGame.route + NavPath.Theme + NavPath.GameMode)
                        currentBackStackEntry?.arguments?.putBoolean(
                            NavPath.GameMode.argument,
                            backStack.arguments?.getBoolean(NavPath.GameMode.argument) == true
                        )
                        currentBackStackEntry?.arguments?.putString(
                            NavPath.Theme.argument,
                            backStack.arguments?.getString(NavPath.Theme.argument)
                        )
                    }
                },
                mainViewModel = activityMainViewModel
            )
        }
        composable(
            route = Screen.CreateRoomGame.route + NavPath.Theme + NavPath.GameMode,
            arguments = listOf(
                navArgument(NavPath.Theme.argument) { nullable = true; type = NavType.StringType },
                navArgument(NavPath.GameMode.argument) { defaultValue = true }
            )
        ) { backStack ->
            RoomCreationView(
                mainViewModel = activityMainViewModel,
                createGame = { roomName ->
                    with(navController) {
                        navigate(Screen.Waiting.route + NavPath.Theme + NavPath.GameMode + NavPath.IsAdmin + NavPath.RoomName)
                        currentBackStackEntry?.arguments?.putBoolean(
                            NavPath.GameMode.argument,
                            backStack.arguments?.getBoolean(NavPath.GameMode.argument) == true
                        )
                        currentBackStackEntry?.arguments?.putString(
                            NavPath.Theme.argument,
                            backStack.arguments?.getString(NavPath.Theme.argument)
                        )
                        currentBackStackEntry?.arguments?.putBoolean(
                            NavPath.IsAdmin.argument,
                            true
                        )
                        currentBackStackEntry?.arguments?.putString(
                            NavPath.RoomName.argument,
                            roomName
                        )
                    }
                }
            )
        }
        composable(
            route = Screen.Waiting.route + NavPath.Theme + NavPath.GameMode + NavPath.IsAdmin + NavPath.RoomName,
            arguments = listOf(
                navArgument(NavPath.Theme.argument) { nullable = true; type = NavType.StringType },
                navArgument(NavPath.GameMode.argument) { defaultValue = true },
                navArgument(NavPath.IsAdmin.argument) { defaultValue = false }
            )
        ) { backStack ->
            WaitingView(
                beginGame = {
                    with(navController) {
                        navigate(Screen.Camera.route + NavPath.Theme + NavPath.GameMode + NavPath.IsAdmin)
                        currentBackStackEntry?.arguments?.putBoolean(
                            NavPath.GameMode.argument,
                            backStack.arguments?.getBoolean(NavPath.GameMode.argument) == true
                        )
                        currentBackStackEntry?.arguments?.putString(
                            NavPath.Theme.argument,
                            backStack.arguments?.getString(NavPath.Theme.argument)
                        )
                        currentBackStackEntry?.arguments?.putBoolean(
                            NavPath.IsAdmin.argument,
                            backStack.arguments?.getBoolean(NavPath.IsAdmin.argument) == true
                        )
                        currentBackStackEntry?.arguments?.putString(
                            NavPath.RoomName.argument,
                            backStack.arguments?.getString(NavPath.RoomName.argument)
                        )
                    }
                },
                showResults =
                {
                    with(navController) {
                        navigate(Screen.Dashboard.route + NavPath.Theme + NavPath.GameMode + NavPath.IsAdmin)
                        currentBackStackEntry?.arguments?.putBoolean(
                            NavPath.GameMode.argument,
                            backStack.arguments?.getBoolean(NavPath.GameMode.argument) == true
                        )
                        currentBackStackEntry?.arguments?.putString(
                            NavPath.Theme.argument,
                            backStack.arguments?.getString(NavPath.Theme.argument)
                        )
                        currentBackStackEntry?.arguments?.putBoolean(
                            NavPath.IsAdmin.argument,
                            backStack.arguments?.getBoolean(NavPath.IsAdmin.argument) == true
                        )
                    }
                },
                isAdmin = backStack.arguments?.getBoolean(NavPath.IsAdmin.argument) == true,
                roomName = backStack.arguments?.getString(NavPath.RoomName.argument)
                    ?: LocaleUtils.UNKNOWN_ROOM_NAME,
                quitGame = { quitGame(navController) },
                mainViewModel = activityMainViewModel
            )
        }

        composable(
            route = Screen.Camera.route + NavPath.Theme + NavPath.GameMode + NavPath.IsAdmin,
            arguments = listOf(
                navArgument(NavPath.Theme.argument) { nullable = true; type = NavType.StringType },
                navArgument(NavPath.GameMode.argument) { defaultValue = true },
                navArgument(NavPath.IsAdmin.argument) { defaultValue = false }
            )
        ) { backStack ->
            CameraView(
                showResults =
                {
                    with(navController) {
                        navigate(Screen.Dashboard.route + NavPath.Theme + NavPath.GameMode + NavPath.IsAdmin)
                        currentBackStackEntry?.arguments?.putBoolean(
                            NavPath.GameMode.argument,
                            backStack.arguments?.getBoolean(NavPath.GameMode.argument) == true
                        )
                        currentBackStackEntry?.arguments?.putString(
                            NavPath.Theme.argument,
                            backStack.arguments?.getString(NavPath.Theme.argument)
                        )
                        currentBackStackEntry?.arguments?.putBoolean(
                            NavPath.IsAdmin.argument,
                            backStack.arguments?.getBoolean(NavPath.IsAdmin.argument) == true
                        )
                    }
                },
                mainViewModel = activityMainViewModel,
                quitGame = { quitGame(navController) }
            )
        }
        composable(
            route = Screen.Dashboard.route + NavPath.Theme + NavPath.GameMode + NavPath.IsAdmin,
            arguments = listOf(
                navArgument(NavPath.Theme.argument) { nullable = true; type = NavType.StringType },
                navArgument(NavPath.GameMode.argument) { defaultValue = true },
                navArgument(NavPath.IsAdmin.argument) { defaultValue = false }
            )
        ) { backStack ->
            DashboardView(
                {
                    with(navController) {
                        navigate(Screen.Camera.route + NavPath.Theme + NavPath.GameMode + NavPath.IsAdmin)
                        currentBackStackEntry?.arguments?.putBoolean(
                            NavPath.GameMode.argument,
                            backStack.arguments?.getBoolean(NavPath.GameMode.argument) == true
                        )
                        currentBackStackEntry?.arguments?.putString(
                            NavPath.Theme.argument,
                            backStack.arguments?.getString(NavPath.Theme.argument)
                        )
                        currentBackStackEntry?.arguments?.putBoolean(
                            NavPath.IsAdmin.argument,
                            backStack.arguments?.getBoolean(NavPath.IsAdmin.argument) == true
                        )
                    }
                },
                quitGame = { quitGame(navController) },
                mainViewModel = activityMainViewModel
            )
        }

        composable(
            route = Screen.ReconnectToRoomGame.route + NavPath.Theme + NavPath.GameMode + NavPath.IsAdmin + NavPath.RoomName,
            arguments = listOf(
                navArgument(NavPath.Theme.argument) { nullable = true; type = NavType.StringType },
                navArgument(NavPath.GameMode.argument) { defaultValue = true },
                navArgument(NavPath.IsAdmin.argument) { defaultValue = false }
            )

        ) { backStack ->
            RoomReconnectionView(
                joinGame = { roomName ->
                    with(navController) {
                        navigate(Screen.Waiting.route + NavPath.Theme + NavPath.GameMode + NavPath.IsAdmin + NavPath.RoomName)
                        currentBackStackEntry?.arguments?.putBoolean(
                            NavPath.GameMode.argument,
                            backStack.arguments?.getBoolean(NavPath.GameMode.argument) == true
                        )
                        currentBackStackEntry?.arguments?.putString(
                            NavPath.Theme.argument,
                            backStack.arguments?.getString(NavPath.Theme.argument)
                        )
                        currentBackStackEntry?.arguments?.putBoolean(
                            NavPath.IsAdmin.argument,
                            true
                        )
                        currentBackStackEntry?.arguments?.putString(
                            NavPath.RoomName.argument,
                            roomName
                        )
                    }
                },
                quitGame = { quitGame(navController) },
                mainViewModel = activityMainViewModel,
                roomName = backStack.arguments?.getString(NavPath.RoomName.argument)
                    ?: LocaleUtils.UNKNOWN_ROOM_NAME,
            )
        }
    }
}