package com.cubifan.vachercher

import android.app.Application
import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.util.Log
import android.widget.Toast
import androidx.compose.material.SnackbarHostState
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateListOf
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.viewModelScope
import com.amplifyframework.core.Amplify
import com.cubifan.vachercher.core.data.PastGame
import com.cubifan.vachercher.core.data.Player
import com.cubifan.vachercher.core.di.DependencyInjector
import com.cubifan.vachercher.core.ws.models.*
import com.cubifan.vachercher.utils.CoroutineTimer
import com.cubifan.vachercher.utils.LocaleUtils
import com.cubifan.vachercher.utils.UserInfos
import com.cubifan.vachercher.utils.showNewSnackbar
import com.tinder.scarlet.WebSocket
import kotlinx.coroutines.Deferred
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.Dispatchers.Main
import kotlinx.coroutines.Job
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.flow.receiveAsFlow
import kotlinx.coroutines.launch
import java.io.File
import java.net.HttpURLConnection

class MainViewModel(
    application: Application,
    savedStateHandle: SavedStateHandle
) : AndroidViewModel(application) {

    sealed class SocketEvent {
        data class AnnouncementEvent(val data: Announcement) : SocketEvent()
        data class FoundedEvent(val data: Founded) : SocketEvent()
        data class GameErrorEvent(val data: GameError) : SocketEvent()
        data class KickPlayerEvent(val data: KickPlayer) : SocketEvent()
        data class GameStateEvent(val data: GameState) : SocketEvent()
        data class RoomPublicDataEvent(val data: RoomPublicData) : SocketEvent()
        data class LeftRoomFoundEvent(val data: LeftRoomFound) : SocketEvent()
        data class JoinRoomHandshakeResponseEvent(val data: JoinRoomHandshakeResponse) :
            SocketEvent()

        data class GamePhaseChangeEvent(val data: GamePhaseChange) : SocketEvent()
    }

    enum class FoundedState {
        PROCESSING, FOUNDED, NOT_FOUNDED
    }

    private var connectionEventsJob: Job? = null
    private var baseModelEventsJob: Job? = null

    private val connectionEventChannel = Channel<WebSocket.Event>()
    val connectionEvent = connectionEventChannel.receiveAsFlow().flowOn(IO)

    private val socketEventChannel = Channel<SocketEvent>()
    val socketEvent = socketEventChannel.receiveAsFlow().flowOn(IO)

    var roomName: String = ""

    val snackbarHostState = mutableStateOf(SnackbarHostState())

    val itemToSearch = mutableStateOf("")
    val leftRoomName = mutableStateOf("")
    var joinRoomHandshakeErrorMessage = mutableStateOf("")

    val foundedState = mutableStateOf(FoundedState.PROCESSING)
    val joinRoomHandshakeResponse = MutableStateFlow(JoinRoomHandshakeResponse(false))

    private val _currentGamePhase = MutableStateFlow(GamePhaseChange(null, 0L))
    val currentGamePhase: MutableStateFlow<GamePhaseChange> = _currentGamePhase

    private val _gamePhaseTime = MutableStateFlow(0L)
    val gamePhaseTime: StateFlow<Long> = _gamePhaseTime

    val roomPublicDataState: MutableState<RoomPublicData?> = mutableStateOf(null)

    private val timer = CoroutineTimer()
    var timerJob: Job? = null

    val popupShowing = mutableStateOf(false)
    val leftRoomFound = mutableStateOf(false)
    val joinRoomHandshakeErrorReceived = mutableStateOf(false)
    val internetIsUnavailable = mutableStateOf(false)

    val gotKicked = MutableStateFlow(false)

    var leaderboardPlayers = mutableStateListOf<Player>()
    var pastGames = mutableStateListOf<PastGame>()
    var firebaseTokenIdExpired = mutableStateOf(false)

    init {
        observeConnectionEvents()
        observeBaseModels()
    }

    fun getUser() {
        viewModelScope.launch(IO) {
            val tag = "GET_USER"
            try {
                val userInformationRetrieval =
                    DependencyInjector.API_SERVICE.getUserByUid(
                        UserInfos.uid,
                        "Bearer " + getApplication<Application>().getSharedPreferences(
                            getApplication<Application>().getString(
                                R.string.firebase_token_shared_preferences,
                                BuildConfig.APPLICATION_ID
                            ),
                            Context.MODE_PRIVATE
                        ).getString(LocaleUtils.FIREBASE_TOKEN, "") as String
                    )

                userInformationRetrieval.body()?.let { response ->
                    if (response.successful) {
                        with(UserInfos) {
                            pseudo = response.data!!.nickname
                            email = response.data.email
                            level = response.data.level
                            score = response.data.totalScore
                            wins = response.data.totalWins
                        }
                    } else {
                        Log.e(tag, response.message!!)
                    }
                } ?: run {
                    Log.e(tag, "Une erreur inattendue s'est produite. Veuillez réessayer.")

                    if (userInformationRetrieval.code() == HttpURLConnection.HTTP_UNAUTHORIZED) {
                        firebaseTokenIdExpired.value = true
                    }
                }
            } catch (e: Exception) {
                Log.e(tag, e.toString())
            }
        }
    }

    fun getWorldLeaderboard() =
        viewModelScope.launch(Main) {
            val tag = "GET_LEADERBOARD"
            try {
                val leaderboardResponse =
                    DependencyInjector.API_SERVICE.getInternationalLeaderboard(
                        "Bearer " + getApplication<Application>().getSharedPreferences(
                            getApplication<Application>().getString(
                                R.string.firebase_token_shared_preferences,
                                BuildConfig.APPLICATION_ID
                            ),
                            Context.MODE_PRIVATE
                        ).getString(LocaleUtils.FIREBASE_TOKEN, "") as String
                    )

                leaderboardResponse.body()?.let { response ->
                    if (response.successful) {
                        leaderboardPlayers.clear()
                        response.data?.forEach { player ->
                            leaderboardPlayers.add(
                                Player(
                                    player.nickname,
                                    player.totalScore,
                                    player.position
                                )
                            )
                        }
                    } else {
                        Log.e(tag, response.message!!)
                    }
                } ?: run {
                    Log.e(tag, "Une erreur inattendue s'est produite. Veuillez réessayer.")

                    if (leaderboardResponse.code() == HttpURLConnection.HTTP_UNAUTHORIZED) {
                        firebaseTokenIdExpired.value = true
                    }
                }
            } catch (e: Exception) {
                Log.e(tag, e.toString())
            }
        }

    fun getPastGames() =
        viewModelScope.launch(Main) {
            val tag = "GET_GAME_HISTORY"
            try {
                val pastGamesResponse =
                    DependencyInjector.API_SERVICE.getPastGames(
                        UserInfos.uid,
                        "Bearer " + getApplication<Application>().getSharedPreferences(
                            getApplication<Application>().getString(
                                R.string.firebase_token_shared_preferences,
                                BuildConfig.APPLICATION_ID
                            ),
                            Context.MODE_PRIVATE
                        ).getString(LocaleUtils.FIREBASE_TOKEN, "") as String
                    )

                pastGamesResponse.body()?.let { response ->
                    if (response.successful) {
                        pastGames.clear()
                        response.data?.forEach { game ->
                            pastGames.add(PastGame(game.score, game.date, game.ranking, game.theme))
                        }
                    } else {
                        Log.e(tag, response.message!!)
                    }
                } ?: run {
                    Log.e(tag, "Une erreur inattendue s'est produite. Veuillez réessayer.")

                    if (pastGamesResponse.code() == HttpURLConnection.HTTP_UNAUTHORIZED) {
                        firebaseTokenIdExpired.value = true
                    }
                }
            } catch (e: Exception) {
                Log.e(tag, e.toString())
            }
        }


    fun assertFirebaseTokenId() = viewModelScope.launch(IO) {
        val tag = "ASSERT_FIREBASE_TOKEN_ID"
        try {
            val assertTokenResponse =
                DependencyInjector.API_SERVICE.assertFirebaseTokenId(
                    "Bearer " + getApplication<Application>().getSharedPreferences(
                        getApplication<Application>().getString(
                            R.string.firebase_token_shared_preferences,
                            BuildConfig.APPLICATION_ID
                        ),
                        Context.MODE_PRIVATE
                    ).getString(LocaleUtils.FIREBASE_TOKEN, "") as String
                )
            assertTokenResponse.body()?.let { response ->
                if (response.successful) {
                    observeConnectionEvents()
                    observeBaseModels()

                    sendBaseModel(
                        ClientConnected(
                            UserInfos.uid,
                            UserInfos.pseudo
                        )
                    )
                }
            } ?: run {
                Log.e(tag, "Une erreur inattendue s'est produite. Veuillez réessayer.")
            }
        } catch (e: Exception) {
            Log.e(tag, e.toString())
        }
    }

    private fun observeBaseModels() {
        baseModelEventsJob?.cancel()

        baseModelEventsJob = viewModelScope.launch(IO) {
            DependencyInjector.scarlet.observeBaseModels()
                .collect { baseModel ->
                    when (baseModel) {
                        is Announcement -> {
                            socketEventChannel.send(SocketEvent.AnnouncementEvent(baseModel))
                        }

                        is Founded -> {
                            socketEventChannel.send(SocketEvent.FoundedEvent(baseModel))
                        }

                        is GameError -> {
                            socketEventChannel.send(SocketEvent.GameErrorEvent(baseModel))
                        }

                        is KickPlayer -> {
                            socketEventChannel.send(SocketEvent.KickPlayerEvent(baseModel))
                            gotKicked.value = true
                        }

                        is GameState -> {
                            socketEventChannel.send(SocketEvent.GameStateEvent(baseModel))
                        }

                        is RoomPublicData -> {
                            socketEventChannel.send(SocketEvent.RoomPublicDataEvent(baseModel))
                        }

                        is GamePhaseChange -> {
                            socketEventChannel.send(SocketEvent.GamePhaseChangeEvent(baseModel))
                        }

                        is Ping -> {
                            sendBaseModel(
                                Ping(UserInfos.uid)
                            )
                        }

                        is LeftRoomFound -> {
                            socketEventChannel.send(SocketEvent.LeftRoomFoundEvent(baseModel))
                        }

                        is JoinRoomHandshakeResponse -> {
                            socketEventChannel.send(
                                SocketEvent.JoinRoomHandshakeResponseEvent(
                                    baseModel
                                )
                            )
                        }
                    }
                }
        }
    }

    fun sendBaseModel(data: BaseModel) {
        viewModelScope.launch(IO) {
            DependencyInjector.scarlet.sendBaseModel(
                data
            )

            if (data is DisconnectionRequest) {
                clearGameState()
            }
        }
    }

    suspend fun prepareAndSendImageToApi(imageKey: String, compressedImage: Deferred<File>) {
        Amplify.Storage.uploadFile(
            imageKey, compressedImage.await(),
            {
                sendBaseModel(
                    ImageSending(
                        roomName = roomName,
                        base64Image = imageKey,
                        userUid = UserInfos.uid
                    )
                )
            },
            { Log.e("MyAmplifyApp", "Upload failed", it) }
        )
    }

    fun collectSocketEvents() {
        viewModelScope.launch(IO) {
            socketEvent.collect {
                when (it) {
                    is SocketEvent.GameErrorEvent -> {
                        Log.e("GAME ERROR", it.data.errorMessage)
                    }

                    is SocketEvent.KickPlayerEvent -> {
                        snackbarHostState.value.showNewSnackbar(it.data.reason, null)
                    }

                    is SocketEvent.AnnouncementEvent -> {
                        snackbarHostState.value.showNewSnackbar(it.data.message, null)
                    }

                    is SocketEvent.FoundedEvent -> {
                        foundedState.value =
                            if (it.data.message.contains("Trouvé")) FoundedState.FOUNDED else FoundedState.NOT_FOUNDED
                        snackbarHostState.value.showSnackbar(it.data.message)
                    }

                    is SocketEvent.GameStateEvent -> {
                        itemToSearch.value = it.data.itemToSearch
                    }

                    is SocketEvent.RoomPublicDataEvent -> {
                        roomPublicDataState.value = it.data
                    }

                    is SocketEvent.LeftRoomFoundEvent -> {
                        leftRoomFound.value = true
                        leftRoomName.value = it.data.roomName
                    }

                    is SocketEvent.JoinRoomHandshakeResponseEvent -> {
                        joinRoomHandshakeResponse.value = it.data

                        if (!it.data.isSuccessful) {
                            joinRoomHandshakeErrorMessage.value = it.data.errorMessage ?: ""
                            joinRoomHandshakeErrorReceived.value = true
                        }
                    }

                    is SocketEvent.GamePhaseChangeEvent -> {
                        it.data.phase?.let { phase ->
                            _currentGamePhase.value = it.data
                        }

                        _gamePhaseTime.value = it.data.time

                        if (it.data.phase != null && it.data.phase != Room.GamePhase.WAITING_FOR_PLAYERS) {
                            setGamePhaseTimer(it.data.time)
                        }
                    }
                }
            }
        }
    }

    fun collectConnectionEvents() {
        viewModelScope.launch(IO) {
            connectionEvent.collect {
                when (it) {
                    is WebSocket.Event.OnConnectionOpened<*> -> {
                        assertFirebaseTokenId()
                        Log.d("OPENED", "ON CONNECTION OPENED")
                    }
                    is WebSocket.Event.OnConnectionFailed -> {
                        Log.d("FAILED", "ON CONNECTION FAILED")
                    }
                    is WebSocket.Event.OnConnectionClosed -> {
                        Log.d("WEBSOCKET", "ON CONNECTION CLOSED")
                    }
                    is WebSocket.Event.OnConnectionClosing -> {
                        Log.d("WEBSOCKET", "ON CONNECTION CLOSING")
                    }
                    is WebSocket.Event.OnMessageReceived -> {
                        Log.d("WEBSOCKET MSG", it.toString())
                    }
                }
            }
        }
    }

    fun copyGame(context: Context, toast: Toast, textToCopy: String) {
        val clipboardManager =
            context.getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
        val clipData = ClipData.newPlainText("Shared game", textToCopy)
        clipboardManager.setPrimaryClip(clipData)
        toast.show()
    }

    private fun setGamePhaseTimer(duration: Long) {
        timerJob?.cancel()
        timerJob = timer.timeAndEmit(duration, viewModelScope) {
            _gamePhaseTime.value = it
        }
    }

    private fun observeConnectionEvents() {
        try {
            connectionEventsJob?.cancel()

            connectionEventsJob = viewModelScope.launch(IO) {
                DependencyInjector.scarlet.observeEvents()
                    .collect { connectionEvent ->
                        connectionEventChannel.send(connectionEvent)
                    }
            }
        } catch (e: Exception) {
            print(e)
        }
    }

    private fun clearGameState() {
        timerJob?.cancel()
        _currentGamePhase.value = GamePhaseChange(null, 0L)
    }
}