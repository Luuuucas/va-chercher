package com.cubifan.vachercher.utils

import com.tinder.scarlet.Lifecycle
import com.tinder.scarlet.lifecycle.LifecycleRegistry

class CustomSocketLifecycle(val lifecycleRegistry: LifecycleRegistry = LifecycleRegistry()) :
    Lifecycle by lifecycleRegistry {
    init {
        lifecycleRegistry.onNext(Lifecycle.State.Started)
    }
}