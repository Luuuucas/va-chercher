package com.cubifan.vachercher.utils

import androidx.compose.material.SnackbarDuration
import androidx.compose.material.SnackbarHostState

suspend fun SnackbarHostState.showNewSnackbar(
    message: String,
    hapticAction: (() -> Unit)?,
) {
    hapticAction?.invoke()
    currentSnackbarData?.dismiss()

    showSnackbar(message, duration = SnackbarDuration.Short)
}
