package com.cubifan.vachercher.utils

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase

object UserInfos {
    var uid: String = ""
        get() = Firebase.auth.currentUser?.uid ?: field
    var pseudo by mutableStateOf("Pseudo")
    var email by mutableStateOf("Email")
    var score by mutableStateOf(0)
    var level by mutableStateOf(1)
    var wins by mutableStateOf(0)
}
