package com.cubifan.vachercher.utils

import android.content.Context
import android.content.SharedPreferences
import android.content.res.Configuration
import java.util.*

object LocaleUtils {

    fun setLocale(c: Context, language: String) = updateResources(c, language)

    private fun updateResources(context: Context, language: String) {
        context.resources.apply {
            val locale = Locale(language)
            val config = Configuration(configuration)

            context.createConfigurationContext(configuration)
            Locale.setDefault(locale)
            config.setLocale(locale)
            context.resources.updateConfiguration(config, displayMetrics)
        }
    }

    fun changeAppLanguage(sharedPrefs: SharedPreferences, language: String) {
        sharedPrefs.edit().putString(LANGUAGE_KEY, language).apply()
    }

    const val AUTOLOGIN = "autologin"
    const val FIREBASE_TOKEN: String = "firebase_token"
    const val LANGUAGE_KEY: String = "language"
    const val ONBOARDING_COMPLETED: String = "onboarding_completed"
    const val UNKNOWN_ROOM_NAME: String = "unknown"
}