package com.cubifan.vachercher.ui.theme

import androidx.compose.ui.graphics.Color

//Material colors
val Orange = Color(0xFFFFC65C)
val DeepOrange = Color(0xFFDC7633)
val DarkOrange = Color(0xFFC08416)
val Brown = Color(0xFFBB8959)
val LightBrown = Color(0xFFE8AA36)
val Error = Color(0xFFCC0000)
val Success = Color(0xFF50C878)

val Gold = Color(0xFFFFD700)
val Silver = Color(0xFFC0C0C0)
val Bronze = Color(0xFFCD7F32)
val Chocolate = Color(0xFF401C00)