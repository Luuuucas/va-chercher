package com.cubifan.vachercher.ui.theme

import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.material.MaterialTheme
import androidx.compose.material.darkColors
import androidx.compose.material.lightColors
import androidx.compose.runtime.Composable
import androidx.compose.ui.graphics.Color

private val DarkColorPalette = darkColors(
    primary = Orange,
    primaryVariant = DarkOrange,
    secondary = Brown,
    secondaryVariant = LightBrown,
    surface = Color.DarkGray,
    background = Color.Gray,
    onBackground = Color.Black,
    error = Error,
    onPrimary = Color.Black,
    onSecondary = Color.White,
    onSurface = Color.White,
)

private val LightColorPalette = lightColors(
    primary = Orange,
    primaryVariant = DarkOrange,
    secondary = Brown,
    secondaryVariant = LightBrown,
    surface = Color.White,
    background = Color.White,
    onBackground = Color.Black,
    error = Error,
    onPrimary = Color.Black,
    onSecondary = Color.Black,
    onSurface = Color.Black,
)

@Composable
fun VaChercherTheme(darkTheme: Boolean = isSystemInDarkTheme(), content: @Composable () -> Unit) {
    val colors = LightColorPalette
    /*
    val colors = if (darkTheme) {
        DarkColorPalette
    } else {
        LightColorPalette
    }
    */

    MaterialTheme(
        colors = colors,
        typography = Typography,
        shapes = Shapes,
        content = content
    )
}