package com.cubifan.vachercher

import android.content.Context
import android.content.SharedPreferences
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.MotionEvent
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavController
import androidx.navigation.compose.rememberNavController
import com.amplifyframework.AmplifyException
import com.amplifyframework.auth.cognito.AWSCognitoAuthPlugin
import com.amplifyframework.core.Amplify
import com.amplifyframework.storage.s3.AWSS3StoragePlugin
import com.cubifan.vachercher.core.components.CustomPopup
import com.cubifan.vachercher.core.data.PastGame
import com.cubifan.vachercher.core.data.Player
import com.cubifan.vachercher.core.ws.models.DisconnectionRequest
import com.cubifan.vachercher.nav.MainNavGraph
import com.cubifan.vachercher.ui.theme.VaChercherTheme
import com.cubifan.vachercher.utils.ConnectivityObserver
import com.cubifan.vachercher.utils.LocaleUtils
import com.cubifan.vachercher.utils.LocaleUtils.AUTOLOGIN
import com.cubifan.vachercher.utils.LocaleUtils.FIREBASE_TOKEN
import com.cubifan.vachercher.utils.LocaleUtils.LANGUAGE_KEY
import com.cubifan.vachercher.utils.NetworkConnectivityObserver
import com.cubifan.vachercher.utils.UserInfos
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import kotlinx.coroutines.launch

class MainActivity : ComponentActivity() {

    private lateinit var firebaseAuth: FirebaseAuth
    private lateinit var activityMainViewModel: MainViewModel
    private lateinit var connectivityObserver: ConnectivityObserver

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        connectivityObserver = NetworkConnectivityObserver(applicationContext)

        try {
            Amplify.addPlugin(AWSCognitoAuthPlugin())
            Amplify.addPlugin(AWSS3StoragePlugin())
            Amplify.configure(applicationContext)
        } catch (e: AmplifyException) {
            Log.e("Amplify", e.message.toString())
        }

        firebaseAuth = Firebase.auth

        val autoLoginSharedPrefs = this.getSharedPreferences(
            getString(R.string.auto_login_shared_preferences, BuildConfig.APPLICATION_ID),
            Context.MODE_PRIVATE
        )
        val parametersSharedPrefs = this.getSharedPreferences(
            getString(R.string.parameters_shared_preferences, BuildConfig.APPLICATION_ID),
            Context.MODE_PRIVATE
        )

        val onboardingSharedPrefs = this.getSharedPreferences(
            getString(R.string.onboarding_shared_preferences, BuildConfig.APPLICATION_ID),
            Context.MODE_PRIVATE
        )

        val firebaseTokenIdSharedPrefs = this.getSharedPreferences(
            getString(R.string.firebase_token_shared_preferences, BuildConfig.APPLICATION_ID),
            Context.MODE_PRIVATE
        )

        parametersSharedPrefs.edit().putString(LANGUAGE_KEY, "fr").apply()
        setContent {
            activityMainViewModel = viewModel()

            firebaseAuth.currentUser?.let {
                UserInfos.uid = it.uid
            }

            val internetStatus by connectivityObserver.observe().collectAsState(
                initial = ConnectivityObserver.Status.Available
            )

            if (internetStatus == ConnectivityObserver.Status.Available) {
                activityMainViewModel.internetIsUnavailable.value = false

                activityMainViewModel.collectSocketEvents()
                activityMainViewModel.collectConnectionEvents()
            } else {
                activityMainViewModel.internetIsUnavailable.value = true
            }

            val languageDefault = parametersSharedPrefs.getString(LANGUAGE_KEY, "en") as String
            LocaleUtils.setLocale(LocalContext.current, languageDefault)

            VaChercherTheme {
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colors.background
                ) {
                    ContentPage(
                        autoLoginSharedPrefs = autoLoginSharedPrefs,
                        onboardingSharedPrefs = onboardingSharedPrefs,
                        firebaseTokenIdSharedPrefs = firebaseTokenIdSharedPrefs,
                        activityMainViewModel = activityMainViewModel,
                        quitGame = { navController ->
                            if (!activityMainViewModel.gotKicked.value) {
                                activityMainViewModel.sendBaseModel(
                                    DisconnectionRequest(
                                        userUid = UserInfos.uid,
                                        isFinalDisconnection = true
                                    )
                                )
                            }

                            activityMainViewModel.getUser()
                            activityMainViewModel.gotKicked.value = false
                            activityMainViewModel.getPastGames()
                            navController.navigate(Screen.Home.route) { popUpTo(0) }
                        },
                        firebaseAuth = firebaseAuth,
                        showSnackbar = {
                            lifecycleScope.launch {
                                activityMainViewModel.snackbarHostState.value.showSnackbar("Compte créé avec succès")
                            }
                        },
                        updateUser = activityMainViewModel::getUser,
                        updateLeaderboard = activityMainViewModel::getWorldLeaderboard,
                        leaderboardPlayers = activityMainViewModel.leaderboardPlayers,
                        updatePastGames = activityMainViewModel::getPastGames,
                        pastGames = activityMainViewModel.pastGames,
                    )
                }
            }
        }
    }

    override fun onStart() {
        super.onStart()
        val currentUser = firebaseAuth.currentUser

        if (currentUser != null)
            Log.w("Firebase Error", "No current user")
    }

    override fun onResume() {
        super.onResume()
        if (::activityMainViewModel.isInitialized) {
            activityMainViewModel.assertFirebaseTokenId()
        }
    }

    override fun onStop() {
        super.onStop()
        if (::activityMainViewModel.isInitialized) {
            activityMainViewModel.sendBaseModel(DisconnectionRequest(UserInfos.uid))
        }
    }

    override fun dispatchTouchEvent(event: MotionEvent?): Boolean =
        event?.pointerCount == 1 && super.dispatchTouchEvent(event)
}

@Composable
fun ContentPage(
    autoLoginSharedPrefs: SharedPreferences,
    onboardingSharedPrefs: SharedPreferences,
    firebaseTokenIdSharedPrefs: SharedPreferences,
    activityMainViewModel: MainViewModel,
    quitGame: (NavController) -> Unit,
    firebaseAuth: FirebaseAuth,
    showSnackbar: () -> Unit,
    updateUser: () -> Unit,
    updateLeaderboard: () -> Unit,
    leaderboardPlayers: List<Player>,
    updatePastGames: () -> Unit,
    pastGames: List<PastGame>,
) {
    val navController = rememberNavController()

    if (activityMainViewModel.internetIsUnavailable.value) {
        navController.navigate(Screen.Home.route) { popUpTo(0) }
    }

    CustomPopup(showing = activityMainViewModel.internetIsUnavailable,
        allowDismissRequest = false,
        titleText = "Connexion perdue.",
        textText = "Vérifie que tu es bien connecté, pour jouer au jeu !",
        confirmButtonAction = {},
        dismissButtonAction = {})


    CustomPopup(showing = activityMainViewModel.firebaseTokenIdExpired,
        allowDismissRequest = false,
        titleText = "Votre session a expiré !",
        textText = "Vous allez être redirigé vers la page de connexion.",
        confirmButtonText = "Ok !",
        confirmButtonAction = {
            activityMainViewModel.firebaseTokenIdExpired.value = false
            firebaseTokenIdSharedPrefs.edit().remove(FIREBASE_TOKEN).apply()
            firebaseAuth.signOut()
            autoLoginSharedPrefs.edit().putBoolean(AUTOLOGIN, false).apply()

            navController.navigate(Screen.Login.route) { popUpTo(0) }
        },
        dismissButtonAction = {})


    CustomPopup(
        showing = activityMainViewModel.leftRoomFound,
        allowDismissRequest = false,
        titleText = "Partie en cours !",
        textText = "Tu étais connecté à une partie, la rejoindre ?",
        confirmButtonText = "Oui",
        dismissButtonText = "Non",

        confirmButtonAction = {
            navController.navigate(Screen.ReconnectToRoomGame.route + NavPath.Theme + NavPath.GameMode + NavPath.IsAdmin + NavPath.RoomName)
            navController.currentBackStackEntry?.arguments?.putString(
                NavPath.RoomName.argument,
                activityMainViewModel.leftRoomName.value
            )

            activityMainViewModel.leftRoomFound.value = false
            activityMainViewModel.leftRoomName.value = ""
        },
        dismissButtonAction = {
            activityMainViewModel.sendBaseModel(
                DisconnectionRequest(
                    userUid = UserInfos.uid,
                    isFinalDisconnection = true
                )
            )
            activityMainViewModel.gotKicked.value = false

            updateUser()
            updatePastGames()

            navController.navigate(Screen.Home.route) { popUpTo(0) }
            activityMainViewModel.leftRoomFound.value = false
            activityMainViewModel.leftRoomName.value = ""
        }
    )

    MainNavGraph(
        navController = navController,
        autoLoginSharedPrefs = autoLoginSharedPrefs,
        onboardingSharedPrefs = onboardingSharedPrefs,
        firebaseTokenIdSharedPrefs = firebaseTokenIdSharedPrefs,
        updatePastGames = updatePastGames,
        updateUser = updateUser,
        updateLeaderboard = updateLeaderboard,
        showSnackbar = showSnackbar,
        quitGame = quitGame,
        firebaseAuth = firebaseAuth,
        pastGames = pastGames,
        leaderboardPlayers = leaderboardPlayers,
        activityMainViewModel = activityMainViewModel,
    )
}

sealed class Screen(val route: String) {
    object Login : Screen("login")
    object Register : Screen("register")
    object Onboarding : Screen("onboarding")
    object Home : Screen("home")
    object Profile : Screen("profile")
    object Badges : Screen("badges")
    object Leaderboard : Screen("leaderboard")
    object ThemeGame : Screen("theme")
    object RoomGame : Screen("room")
    object CreateRoomGame : Screen("create_room")
    object ReconnectToRoomGame : Screen("reconnect_room")
    object Waiting : Screen("waiting")
    object Camera : Screen("camera")
    object Dashboard : Screen("dashboard")
}

sealed class NavPath(val argument: String) {
    object GameMode : NavPath("/{gameMode}")
    object Theme : NavPath("?themeId={themeId}")
    object IsAdmin : NavPath("&isAdmin={isAdmin}")
    object RoomName : NavPath("&roomName={roomName}")
}

val EMPTY_IMAGE_URI: Uri = Uri.parse("file://dev/null")
