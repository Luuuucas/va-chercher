package com.cubifan.vachercher

import android.app.Application
import com.cubifan.vachercher.core.di.DependencyInjector

class VachercherApplication : Application() {
    override fun onCreate() {
        super.onCreate()
        DependencyInjector.init(this)
    }
}
