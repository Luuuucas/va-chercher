package com.cubifan.vachercher.core.api.response

data class WordsResponse(
    val data: WordResponse,
    val successful: Boolean
)

data class WordResponse(
    val id: Int,
    val word: String,
    val themeId: Int
)
