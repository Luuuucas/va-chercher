package com.cubifan.vachercher.core.components

import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.Shape
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.cubifan.vachercher.R
import com.cubifan.vachercher.ui.theme.Orange

@Composable
fun CustomTimer(
    time: Long,
    round: Int,
    modifier: Modifier,
    shape: Shape = MaterialTheme.shapes.medium
) {
    Card(
        backgroundColor = Orange,
        modifier = modifier,
        shape = shape,
    ) {
        Column(
            modifier = Modifier
                .padding(8.dp)
                .width(IntrinsicSize.Max),
            verticalArrangement = Arrangement.spacedBy(2.dp),
        ) {
            Text(
                text = "Round $round",
                modifier = Modifier.fillMaxWidth(),
                textAlign = TextAlign.Center,
                color = Color.White,
            )
            Divider(color = Color.White)
            Row(verticalAlignment = Alignment.CenterVertically) {
                Icon(painterResource(R.drawable.ic_timer), null, tint = Color.White)
                Text(text = "${time}s", fontSize = 20.sp, color = Color.White)
            }
        }
    }
}

@Preview
@Composable
fun CustomTimerPreview() {
    CustomTimer(time = 15, round = 1, modifier = Modifier)
}