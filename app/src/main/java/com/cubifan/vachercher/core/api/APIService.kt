package com.cubifan.vachercher.core.api

import com.cubifan.vachercher.core.api.requests.RoomRequest
import com.cubifan.vachercher.core.api.requests.UserCreationRequest
import com.cubifan.vachercher.core.api.requests.UserRequest
import com.cubifan.vachercher.core.api.response.*
import retrofit2.Response
import retrofit2.http.*

interface APIService {
    @GET("theme/rooms")
    suspend fun getRoomsByTheme(
        @Query("themeId") themeId: String,
        @Header("Authorization") firebaseToken: String
    ): Response<RoomsResponse>

    @POST("room")
    suspend fun createRoom(
        @Body roomData: RoomRequest,
        @Header("Authorization") firebaseToken: String
    ): Response<RoomCreatedResponse>

    @GET("themes")
    suspend fun getThemes(@Header("Authorization") firebaseToken: String): Response<ThemesResponse>

    @POST("/login")
    suspend fun login(@Body userData: UserRequest): Response<UserResponse>

    @POST("/register")
    suspend fun register(@Body userData: UserCreationRequest): Response<UserResponse>

    @DELETE("/user")
    suspend fun delete(@Query("userUid") userUid: String): Response<DeleteUserResponse>

    @GET("user")
    suspend fun getUserByUid(
        @Query("uid") themeId: String,
        @Header("Authorization") firebaseToken: String
    ): Response<UserResponse>

    @GET("ranking/international")
    suspend fun getInternationalLeaderboard(@Header("Authorization") firebaseToken: String): Response<LeaderboardResponse>

    @GET("user/game-history")
    suspend fun getPastGames(
        @Query("uid") userUid: String,
        @Header("Authorization") firebaseToken: String
    ): Response<GameHistoryResponse>

    @GET("token-assertion")
    suspend fun assertFirebaseTokenId(
        @Header("Authorization") firebaseToken: String
    ): Response<TokenAssertionResponse>
}