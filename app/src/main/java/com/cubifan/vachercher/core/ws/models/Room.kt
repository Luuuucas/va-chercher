package com.cubifan.vachercher.core.ws.models

data class Room(
    val name: String,
    val maxPlayers: Int,
    val playerCount: Int = 1
) {
    enum class GamePhase {
        WAITING_FOR_PLAYERS,
        WAITING_FOR_START,
        SEARCH,
        SHOW_RESULTS
    }
}