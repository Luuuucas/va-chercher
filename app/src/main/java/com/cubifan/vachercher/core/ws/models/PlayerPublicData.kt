package com.cubifan.vachercher.core.ws.models

data class PlayerPublicData(
    val username: String,
    val score: Int = 0,
    var rank: Int = 0
)
