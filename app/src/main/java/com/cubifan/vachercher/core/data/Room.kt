package com.cubifan.vachercher.core.data

data class Room(
    val name: String,
    val isPrivate: Boolean,
    val maxPlayers: Int,
    val connectedPlayers: Int,
    val themeId: Int
) {
    constructor() : this("", false, 0, 0, 0)
}