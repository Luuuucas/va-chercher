package com.cubifan.vachercher.core.components

import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Button
import androidx.compose.material.ButtonDefaults
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.TextUnit
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.cubifan.vachercher.ui.theme.Orange

@Composable
fun DownRoundButton(
    onClick: () -> Unit,
    text: String,
    modifier: Modifier = Modifier,
    fontSize: TextUnit = 14.sp,
    color: Color = Orange
) {
    Button(
        onClick = onClick,
        colors = ButtonDefaults.buttonColors(backgroundColor = color),
        modifier = modifier
            .fillMaxWidth(.85f)
            .height(50.dp),
        shape = RoundedCornerShape(15.dp)
    ) {
        Text(text = text, color = Color.White, fontSize = fontSize)
    }
}
