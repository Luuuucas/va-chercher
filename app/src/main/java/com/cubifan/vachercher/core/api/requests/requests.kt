package com.cubifan.vachercher.core.api.requests

data class RoomRequest(
    val name: String,
    val themeId: String,
    val maxPlayers: Int,
    val isPrivate: Boolean,
    val numberOfRounds: Int,
    val password: String? = null
)

data class UserRequest(
    val uid: String
)