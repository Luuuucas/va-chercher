package com.cubifan.vachercher.core.components

import androidx.compose.animation.core.LinearEasing
import androidx.compose.animation.core.RepeatMode
import androidx.compose.animation.core.StartOffset
import androidx.compose.animation.core.StartOffsetType
import androidx.compose.animation.core.animateFloat
import androidx.compose.animation.core.infiniteRepeatable
import androidx.compose.animation.core.rememberInfiniteTransition
import androidx.compose.animation.core.tween
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.offset
import androidx.compose.foundation.layout.size
import androidx.compose.material.Icon
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.rotate
import androidx.compose.ui.draw.scale
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalConfiguration
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.cubifan.vachercher.R
import com.cubifan.vachercher.ui.theme.Orange
import kotlin.random.Random

@Composable
private fun RunningPerson(milisX: Int, milisY: Int, color: Color) {
    val screenSize = LocalConfiguration.current
    val infiniteTransition = rememberInfiniteTransition()
    val sizeIcon = 50
    val degrees = infiniteTransition.animateFloat(
        initialValue = -20f,
        targetValue = 20f,
        animationSpec = infiniteRepeatable(
            tween(Random.nextInt(800, 1300), easing = LinearEasing),
            RepeatMode.Reverse
        )
    )
    val x = infiniteTransition.animateFloat(
        initialValue = 0f,
        targetValue = screenSize.screenWidthDp.toFloat() - sizeIcon,
        animationSpec = infiniteRepeatable(
            animation = tween(milisX),
            repeatMode = RepeatMode.Reverse,
            initialStartOffset = StartOffset(Random.nextInt(0, 10000), StartOffsetType.FastForward)
        )
    )
    val y = infiniteTransition.animateFloat(
        initialValue = 0f,
        targetValue = screenSize.screenHeightDp.toFloat() - sizeIcon,
        animationSpec = infiniteRepeatable(
            animation = tween(milisY),
            repeatMode = RepeatMode.Reverse,
            initialStartOffset = StartOffset(Random.nextInt(0, 10000), StartOffsetType.FastForward)
        )
    )
    Icon(
        painter = painterResource(id = R.drawable.running_person),
        contentDescription = null,
        tint = color,
        modifier = Modifier
            .size(sizeIcon.dp)
            .offset(x.value.dp, y.value.dp)
            .rotate(degrees.value)
            .scale(
                scaleX = if (x.value > screenSize.screenWidthDp / 2.toFloat()) -1f else 1f,
                scaleY = 1f
            )
    )
}

@Composable
fun RunningPeople(color: Color = Orange) {
    Box(Modifier.fillMaxSize()) {
        repeat(3) {
            val x = Random.nextInt(5000, 20000)
            val y = Random.nextInt(8000, 25000)
            RunningPerson(x, y, color)
        }
    }
}

@Preview
@Composable
fun RunningPeoplePreview() {
    RunningPeople()
}
