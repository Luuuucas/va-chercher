package com.cubifan.vachercher.core.ws.models

import com.cubifan.vachercher.core.ws.WsConstants.WS_GAME_ERROR

data class GameError(
    val errorMessage: String,
) : BaseModel(WS_GAME_ERROR)