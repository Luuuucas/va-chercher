package com.cubifan.vachercher.core.api.requests

data class UserCreationRequest(
    val uid: String,
    val nickname: String,
    val email: String
)
