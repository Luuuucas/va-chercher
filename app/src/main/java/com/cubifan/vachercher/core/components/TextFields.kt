package com.cubifan.vachercher.core.components

import android.annotation.SuppressLint
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.relocation.BringIntoViewRequester
import androidx.compose.foundation.relocation.bringIntoViewRequester
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.MaterialTheme
import androidx.compose.material.OutlinedTextField
import androidx.compose.material.Snackbar
import androidx.compose.material.SnackbarHost
import androidx.compose.material.SnackbarHostState
import androidx.compose.material.Text
import androidx.compose.material.TextFieldDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.FocusDirection
import androidx.compose.ui.focus.FocusManager
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalFocusManager
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.input.PasswordVisualTransformation
import androidx.compose.ui.text.input.TextFieldValue
import androidx.compose.ui.text.input.VisualTransformation
import androidx.compose.ui.tooling.preview.Preview
import com.cubifan.vachercher.R.drawable
import com.cubifan.vachercher.ui.theme.Error

@Composable
fun Credentials(
    login: MutableState<TextFieldValue>,
    password: MutableState<TextFieldValue>,
    snackbarHostState: SnackbarHostState,
    passwordAction: ImeAction = ImeAction.Done,
    focusManager: FocusManager = LocalFocusManager.current
) {
    SnackbarHost(
        hostState = snackbarHostState,
        snackbar = { Snackbar(it, backgroundColor = Error) }
    )
    CustomTextField(
        label = "Email",
        placeholder = "Mail",
        textFieldData = login,
        keyboardOptions = KeyboardOptions.Default.copy(
            autoCorrect = false,
            keyboardType = KeyboardType.Email,
            imeAction = ImeAction.Next
        ),
        focusManager
    )
    MiniSpacer()
    CustomTextField(
        label = "Mot de passe",
        placeholder = "",
        textFieldData = password,
        keyboardOptions = KeyboardOptions.Default.copy(
            autoCorrect = false,
            keyboardType = KeyboardType.Password,
            imeAction = passwordAction
        ),
        focusManager
    )
}

@OptIn(ExperimentalFoundationApi::class)
@Composable
fun CustomTextField(
    label: String,
    placeholder: String,
    textFieldData: MutableState<TextFieldValue>,
    keyboardOptions: KeyboardOptions,
    focusManager: FocusManager = LocalFocusManager.current,
    maxSize: Int = Int.MAX_VALUE,
) {
    val isPassword = keyboardOptions.keyboardType == KeyboardType.Password
    val (passwordVisible, setPasswordVisible) = remember { mutableStateOf(!isPassword) }

    OutlinedTextField(
        label = { Text(label) },
        value = textFieldData.value,
        singleLine = true,
        visualTransformation = if (passwordVisible) VisualTransformation.None else PasswordVisualTransformation(),
        onValueChange = {
            textFieldData.value = it.copy(text = it.text.take(maxSize))
        },
        placeholder = { Text(placeholder) },
        keyboardOptions = keyboardOptions,
        keyboardActions = KeyboardActions(
            onNext = {
                focusManager.moveFocus(FocusDirection.Next)
            },
            onDone = {
                focusManager.clearFocus()
            }
        ),
        colors = TextFieldDefaults.textFieldColors(
            textColor = MaterialTheme.colors.onSurface,
            backgroundColor = MaterialTheme.colors.surface,
            focusedLabelColor = Color.Black,
            unfocusedIndicatorColor = MaterialTheme.colors.primary,
            focusedIndicatorColor = Color.Gray
        ),
        trailingIcon = {
            if (isPassword) {
                val iconDrawableId = when (passwordVisible) {
                    true -> drawable.ic_baseline_visibility_24
                    false -> drawable.ic_baseline_visibility_off_24
                }
                val description = if (passwordVisible) "Visible" else "Hidden"

                IconButton(onClick = { setPasswordVisible(!passwordVisible) }) {
                    Icon(painter = painterResource(id = iconDrawableId), description)
                }
            }
        },
        modifier = Modifier
            .fillMaxWidth()
            .bringIntoViewRequester(BringIntoViewRequester())
    )
}

@SuppressLint("UnrememberedMutableState")
@Preview
@Composable
fun CredentialsPreview() {
    val login = mutableStateOf(TextFieldValue())
    val password = mutableStateOf(TextFieldValue())
    Column {
        Credentials(login, password, SnackbarHostState())
    }
}

@Preview
@Composable
fun CustomTextFieldPreview() {
    val loginTextFieldData = remember { mutableStateOf(TextFieldValue()) }
    val passwordTextFieldData = remember { mutableStateOf(TextFieldValue()) }
    Column {
        CustomTextField(
            label = "login",
            placeholder = "type here",
            textFieldData = loginTextFieldData,
            keyboardOptions = KeyboardOptions.Default.copy(
                autoCorrect = false,
                keyboardType = KeyboardType.Email,
                imeAction = ImeAction.Next
            )
        )
        CustomTextField(
            label = "password",
            placeholder = "type here",
            textFieldData = passwordTextFieldData,
            keyboardOptions = KeyboardOptions.Default.copy(
                autoCorrect = false,
                keyboardType = KeyboardType.Password,
                imeAction = ImeAction.Done
            )
        )
    }
}