package com.cubifan.vachercher.core.api.response

import java.net.URL

data class ThemesResponse(
    val data: Array<ThemeData>,
    val successful: Boolean
)

data class ThemeData(
    val id: Int,
    val name: String,
    val description: String,
    val urlPhoto: URL,
    val levelRequired: Int?
)