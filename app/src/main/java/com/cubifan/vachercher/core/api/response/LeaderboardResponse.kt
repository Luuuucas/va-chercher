package com.cubifan.vachercher.core.api.response

data class LeaderboardResponse(
    val data: List<LeaderboardData>?,
    val successful: Boolean,
    val message: String?,
)

data class LeaderboardData(
    val nickname: String,
    val totalScore: Long,
    val position: Int,
)
