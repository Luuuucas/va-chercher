package com.cubifan.vachercher.core.ws.models

import com.cubifan.vachercher.core.ws.WsConstants.WS_GAMEPHASE_CHANGE

data class GamePhaseChange(
    val phase: Room.GamePhase?,
    val time: Long,
) : BaseModel(WS_GAMEPHASE_CHANGE)