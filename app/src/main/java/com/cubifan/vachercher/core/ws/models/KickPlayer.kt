package com.cubifan.vachercher.core.ws.models

import com.cubifan.vachercher.core.ws.WsConstants.WS_KICK_PLAYER

data class KickPlayer(
    val reason: String
) : BaseModel(WS_KICK_PLAYER)
