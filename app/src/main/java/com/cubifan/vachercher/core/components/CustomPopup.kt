package com.cubifan.vachercher.core.components

import android.annotation.SuppressLint
import androidx.compose.material.AlertDialog
import androidx.compose.material.Button
import androidx.compose.material.ButtonDefaults
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import com.cubifan.vachercher.ui.theme.Error

@Composable
fun CustomPopup(
    showing: MutableState<Boolean>,
    allowDismissRequest: Boolean,
    titleText: String,
    textText: String,
    confirmButtonText: String? = null,
    dismissButtonText: String? = null,
    confirmButtonAction: () -> Unit,
    dismissButtonAction: () -> Unit
) {
    if (showing.value) {
        AlertDialog(
            onDismissRequest = { if (allowDismissRequest) showing.value = false },
            confirmButton = {
                if (confirmButtonText != null) {
                    Button(
                        onClick = { showing.value = false; confirmButtonAction() },
                        colors = ButtonDefaults.buttonColors(Error)
                    ) { Text(confirmButtonText) }
                }
            },
            dismissButton = {
                if (dismissButtonText != null) {

                    Button(onClick = { showing.value = false; dismissButtonAction() }) {
                        Text(
                            dismissButtonText
                        )
                    }
                }
            },
            title = { Text(text = titleText, color = Color.Black, fontWeight = FontWeight.Bold) },
            text = { Text(text = textText, color = Color.Black) })
    }
}

@SuppressLint("UnrememberedMutableState")
@Preview
@Composable
fun CustomPopupPreview() {
    val showing = mutableStateOf(true)
    CustomPopup(
        showing,
        true,
        "Quitter ?",
        "Es-tu sûr de quitter la partie?",
        "Oui",
        "Non",
        {},
        {}
    )
}