package com.cubifan.vachercher.core.components

import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Divider
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.cubifan.vachercher.core.data.PastGame
import com.cubifan.vachercher.core.data.Player
import com.cubifan.vachercher.ui.theme.DeepOrange
import com.cubifan.vachercher.utils.UserInfos

@Composable
fun UserDisplay(modifier: Modifier = Modifier, pseudo: String, level: Int) {
    Box(
        modifier = modifier
            .fillMaxWidth()
            .background(MaterialTheme.colors.primary)
            .height(75.dp)
    ) {
        Text(
            modifier = Modifier.align(Alignment.TopCenter),
            text = pseudo,
            fontSize = 32.sp,
            fontWeight = FontWeight.Bold,
        )
        Text(
            text = "Niv. $level",
            fontWeight = FontWeight.Bold,

            modifier = Modifier
                .align(Alignment.BottomEnd)
                .clip(RoundedCornerShape(topStart = 8.dp))
                .background(DeepOrange)
                .padding(8.dp),
            fontSize = 16.sp,
            color = Color.White
        )

    }
}

@Composable
fun <T> Leaderboard(header: List<String>, rows: List<T>) {
    require(rows.isEmpty() || rows.first() is Player || rows.first() is PastGame) { "unknown type" }
    Column(
        modifier = Modifier
            .fillMaxSize(.9f)
            .padding(top = 32.dp)
            .border(
                width = 2.dp,
                color = MaterialTheme.colors.primary,
                shape = RoundedCornerShape(16.dp)
            )
    ) {
        Row(
            modifier = Modifier
                .fillMaxWidth()
                .padding(vertical = 12.dp)
        ) {
            for (text in header) {
                Text(
                    text = text,
                    textAlign = TextAlign.Center,
                    modifier = Modifier.weight(1f),
                    fontWeight = FontWeight.Bold
                )
            }
        }
        Divider(
            color = MaterialTheme.colors.primary,
            thickness = 2.dp,
        )
        LazyColumn {
            items(rows) { rowData ->
                when (rowData) {
                    is PastGame -> {
                        Row(
                            modifier = Modifier.padding(16.dp),
                            verticalAlignment = Alignment.CenterVertically
                        ) {
                            Text(
                                text = rowData.date,
                                textAlign = TextAlign.Center,
                                modifier = Modifier.weight(1f)
                            )
                            Text(
                                text = rowData.theme,
                                textAlign = TextAlign.Center,
                                modifier = Modifier.weight(1f)
                            )
                            Text(
                                text = "${rowData.score}",
                                textAlign = TextAlign.Center,
                                color = MaterialTheme.colors.primary,
                                fontWeight = FontWeight.Bold,
                                modifier = Modifier.weight(1f)
                            )
                            Text(
                                text = rowData.ranking,
                                textAlign = TextAlign.Center,
                                modifier = Modifier.weight(1f)
                            )
                        }
                    }
                    is Player -> {
                        Row(
                            modifier = Modifier
                                .clip(RoundedCornerShape(4.dp))
                                .background(
                                    if (rowData.name == UserInfos.pseudo) MaterialTheme.colors.primary else MaterialTheme.colors.background
                                )
                                .padding(8.dp),
                            verticalAlignment = Alignment.CenterVertically
                        ) {
                            Text(
                                text = "#" + rowData.position.toString(),
                                fontSize = 18.sp,
                                color = if (rowData.name == UserInfos.pseudo) Color.Black else Color.Gray,
                                fontWeight = FontWeight.Bold,
                                modifier = Modifier.weight(.5f)
                            )
                            Text(
                                text = rowData.name,
                                textAlign = TextAlign.Center,
                                fontWeight = FontWeight.Bold,
                                modifier = Modifier.weight(1f)
                            )
                            Text(
                                text = rowData.score.toString(),
                                textAlign = TextAlign.Right,
                                fontSize = 18.sp,
                                color = if (rowData.name == UserInfos.pseudo) Color.White else MaterialTheme.colors.primary,
                                fontWeight = FontWeight.Bold,
                                modifier = Modifier.weight(.5f)
                            )
                        }
                    }
                }
                Divider(color = Color(0x11222222), modifier = Modifier.padding(horizontal = 16.dp))
            }
            if (rows.isEmpty()) {
                item {
                    Text(
                        text = "Tu trouveras ici ton historique",
                        textAlign = TextAlign.Center,
                        modifier = Modifier.fillMaxWidth()
                    )
                }
            }
        }
    }
}

@Preview
@Composable
private fun UserDisplayPreview() {
    Box(contentAlignment = Alignment.Center, modifier = Modifier.fillMaxWidth()) {
        UserDisplay(pseudo = "Lucas", level = 10)
    }
}