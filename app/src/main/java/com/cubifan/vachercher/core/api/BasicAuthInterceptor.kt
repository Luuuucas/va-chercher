package com.cubifan.vachercher.core.api

import android.util.Log
import okhttp3.Credentials
import okhttp3.Interceptor
import okhttp3.Response

class BasicAuthInterceptor(private val username: String, private val password: String): Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        val request = chain.request()
        val basicAuth = Credentials.basic(username, password)
        val auth = request
            .newBuilder()
            .header("Authorization", basicAuth)
            .build()
        return chain.proceed(auth)
    }
}