package com.cubifan.vachercher.core.ws.models

import com.cubifan.vachercher.core.ws.WsConstants.WS_CLIENT_CONNECTED

data class ClientConnected(
    val userUid: String,
    val username: String
) : BaseModel(WS_CLIENT_CONNECTED)