package com.cubifan.vachercher.core.api.response

data class GameHistoryResponse(
    val data: List<GameHistoryData>?,
    val successful: Boolean,
    val message: String?
)

data class GameHistoryData(
    val score: Int,
    val date: String,
    val ranking: String,
    val theme: String,
)
