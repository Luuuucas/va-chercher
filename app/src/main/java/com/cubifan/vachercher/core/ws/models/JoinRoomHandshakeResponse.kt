package com.cubifan.vachercher.core.ws.models

import com.cubifan.vachercher.core.ws.WsConstants.WS_JOIN_ROOM_HANDSHAKE_RESPONSE

data class JoinRoomHandshakeResponse(
    val isSuccessful: Boolean,
    val errorMessage: String? = null
) : BaseModel(WS_JOIN_ROOM_HANDSHAKE_RESPONSE)
