package com.cubifan.vachercher.core.ws.models

import com.cubifan.vachercher.core.ws.WsConstants.WS_GAME_STATE

data class GameState(
    val itemToSearch: String,
) : BaseModel(WS_GAME_STATE)