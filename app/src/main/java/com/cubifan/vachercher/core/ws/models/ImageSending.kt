package com.cubifan.vachercher.core.ws.models

import com.cubifan.vachercher.core.ws.WsConstants.WS_IMAGE_SENDING

data class ImageSending(
    val roomName: String,
    val base64Image: String,
    val userUid: String
) : BaseModel(WS_IMAGE_SENDING)