package com.cubifan.vachercher.core.api.response

data class TokenAssertionResponse(
    val successful: Boolean,
    val message: String,
)
