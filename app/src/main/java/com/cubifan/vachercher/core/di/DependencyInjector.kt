package com.cubifan.vachercher.core.di

import android.app.Application
import com.cubifan.vachercher.BuildConfig
import com.cubifan.vachercher.core.api.APIService
import com.cubifan.vachercher.core.ws.CustomGsonMessageAdapter
import com.cubifan.vachercher.core.ws.FlowStreamAdapter
import com.cubifan.vachercher.core.ws.WsApiService
import com.cubifan.vachercher.utils.CustomSocketLifecycle
import com.cubifan.vachercher.utils.UserInfos
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.tinder.scarlet.Scarlet
import com.tinder.scarlet.lifecycle.android.AndroidLifecycle
import com.tinder.scarlet.retry.ExponentialWithJitterBackoffStrategy
import com.tinder.scarlet.websocket.okhttp.newWebSocketFactory
import kotlinx.coroutines.ExperimentalCoroutinesApi
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.*

object DependencyInjector {
    private val BASE_WS_URL: String = BuildConfig.LINK_WS
    private val BASE_URL = BuildConfig.LINK_API

    private lateinit var appContext: Application

    fun init(appContext: Application) {
        this.appContext = appContext
    }

    private val gson: Gson by lazy {
        GsonBuilder().setLenient().create()
    }

    val httpClient: OkHttpClient by lazy {
        OkHttpClient
            .Builder()
            .addInterceptor { chain ->
                val url = chain.request().url.newBuilder()
                    .addQueryParameter("client_id", UserInfos.uid)
                    .build()

                val request = chain.request().newBuilder()
                    .url(url)
                    .build()
                chain.proceed(request)
            }
            .addInterceptor(HttpLoggingInterceptor().apply {
                level = HttpLoggingInterceptor.Level.NONE
            })
            .build()
    }

    @OptIn(ExperimentalCoroutinesApi::class)
    val scarlet: WsApiService by lazy {
        Scarlet.Builder()
            .backoffStrategy(ExponentialWithJitterBackoffStrategy(500L, 1000L, Random()))
            .webSocketFactory(
                httpClient.newWebSocketFactory(BASE_WS_URL)
            )
            .addStreamAdapterFactory(FlowStreamAdapter.Factory)
            .addMessageAdapterFactory(CustomGsonMessageAdapter.Factory(gson))
            .lifecycle(AndroidLifecycle.ofApplicationForeground(appContext))
            .build()
            .create()
    }

    val scarletLifecycle = CustomSocketLifecycle()

    private val retrofit: Retrofit by lazy {
        Retrofit.Builder()
            .baseUrl(BASE_URL)
            .client(httpClient)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build()
    }

    val API_SERVICE: APIService by lazy {
        retrofit.create(APIService::class.java)
    }
}