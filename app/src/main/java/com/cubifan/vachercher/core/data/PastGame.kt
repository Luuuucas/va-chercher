package com.cubifan.vachercher.core.data

data class PastGame(
    val score: Int,
    val date: String,
    val ranking: String,
    val theme: String,
)
