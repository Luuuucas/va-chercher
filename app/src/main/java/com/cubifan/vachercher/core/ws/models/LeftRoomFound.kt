package com.cubifan.vachercher.core.ws.models

import com.cubifan.vachercher.core.ws.WsConstants.WS_LEFT_ROOM_FOUND

data class LeftRoomFound(
    val roomName: String
) : BaseModel(WS_LEFT_ROOM_FOUND)