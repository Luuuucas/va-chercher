package com.cubifan.vachercher.core.ws.models

import com.cubifan.vachercher.core.ws.WsConstants.WS_IS_FOUNDED

data class Founded(
    val message: String,
) : BaseModel(WS_IS_FOUNDED)
