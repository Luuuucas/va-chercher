package com.cubifan.vachercher.core.components

import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.height
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp

@Composable
fun MidSpacer() = Spacer(Modifier.height(64.dp))

@Composable
fun LittleSpacer() = Spacer(Modifier.height(16.dp))

@Composable
fun MiniSpacer() = Spacer(Modifier.height(4.dp))