package com.cubifan.vachercher.core.ws.models

import com.cubifan.vachercher.core.ws.WsConstants.WS_PING

data class Ping(
    val userUid: String
) : BaseModel(WS_PING)