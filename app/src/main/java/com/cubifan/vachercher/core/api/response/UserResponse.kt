package com.cubifan.vachercher.core.api.response

data class UserResponse(
    val data: UserData?,
    val successful: Boolean,
    val message: String?,
)

data class UserData(
    val id: Int,
    val uid: String,
    val nickname: String,
    val totalScore: Int,
    val email: String,
    val totalWins: Int,
    val level: Int
)

data class DeleteUserResponse(
    val data: String,
    val successful: Boolean,
)
