package com.cubifan.vachercher.core.ws

import com.cubifan.vachercher.core.ws.WsConstants.WS_ANNOUNCEMENT_MESSAGE
import com.cubifan.vachercher.core.ws.WsConstants.WS_CLIENT_CONNECTED
import com.cubifan.vachercher.core.ws.WsConstants.WS_DISCONNECTION_REQUEST
import com.cubifan.vachercher.core.ws.WsConstants.WS_GAMEPHASE_CHANGE
import com.cubifan.vachercher.core.ws.WsConstants.WS_GAME_ERROR
import com.cubifan.vachercher.core.ws.WsConstants.WS_GAME_STATE
import com.cubifan.vachercher.core.ws.WsConstants.WS_IMAGE_SENDING
import com.cubifan.vachercher.core.ws.WsConstants.WS_IS_FOUNDED
import com.cubifan.vachercher.core.ws.WsConstants.WS_JOIN_ROOM_HANDSHAKE
import com.cubifan.vachercher.core.ws.WsConstants.WS_JOIN_ROOM_HANDSHAKE_RESPONSE
import com.cubifan.vachercher.core.ws.WsConstants.WS_KICK_PLAYER
import com.cubifan.vachercher.core.ws.WsConstants.WS_LEFT_ROOM_FOUND
import com.cubifan.vachercher.core.ws.WsConstants.WS_MANUAL_LAUNCH
import com.cubifan.vachercher.core.ws.WsConstants.WS_PING
import com.cubifan.vachercher.core.ws.WsConstants.WS_ROOM_PUBLIC_DATA
import com.cubifan.vachercher.core.ws.models.*
import com.google.gson.Gson
import com.google.gson.JsonParser
import com.tinder.scarlet.Message
import com.tinder.scarlet.MessageAdapter
import java.lang.reflect.Type

class CustomGsonMessageAdapter<T> private constructor(
    gson: Gson
) : MessageAdapter<T> {

    private val gson = Gson()

    override fun fromMessage(message: Message): T {
        val stringValue = when (message) {
            is Message.Text -> message.value
            is Message.Bytes -> message.value.toString()
        }
        val jsonObject = JsonParser.parseString(stringValue).asJsonObject

        val type = when (jsonObject.get("type").asString) {

            WS_ANNOUNCEMENT_MESSAGE -> Announcement::class.java
            WS_IS_FOUNDED -> Founded::class.java
            WS_JOIN_ROOM_HANDSHAKE -> JoinRoomHandshake::class.java
            WS_GAME_ERROR -> GameError::class.java
            WS_IMAGE_SENDING -> ImageSending::class.java
            WS_GAMEPHASE_CHANGE -> GamePhaseChange::class.java
            WS_KICK_PLAYER -> KickPlayer::class.java
            WS_GAME_STATE -> GameState::class.java
            WS_ROOM_PUBLIC_DATA -> RoomPublicData::class.java
            WS_PING -> Ping::class.java
            WS_DISCONNECTION_REQUEST -> Announcement::class.java
            WS_LEFT_ROOM_FOUND -> LeftRoomFound::class.java
            WS_JOIN_ROOM_HANDSHAKE_RESPONSE -> JoinRoomHandshakeResponse::class.java

            else -> BaseModel::class.java
        }
        val obj = gson.fromJson(stringValue, type)
        return obj as T
    }

    override fun toMessage(data: T): Message {
        var convertedData = data as BaseModel
        convertedData = when (convertedData.type) {
            WS_ANNOUNCEMENT_MESSAGE -> convertedData as Announcement
            WS_JOIN_ROOM_HANDSHAKE -> convertedData as JoinRoomHandshake
            WS_GAME_ERROR -> convertedData as GameError
            WS_IMAGE_SENDING -> convertedData as ImageSending
            WS_GAMEPHASE_CHANGE -> convertedData as GamePhaseChange
            WS_GAME_STATE -> convertedData as GameState
            WS_ROOM_PUBLIC_DATA -> convertedData as RoomPublicData
            WS_PING -> convertedData as Ping
            WS_DISCONNECTION_REQUEST -> convertedData as DisconnectionRequest
            WS_MANUAL_LAUNCH -> convertedData as ManualLaunchRequest
            WS_CLIENT_CONNECTED -> convertedData as ClientConnected

            else -> convertedData
        }
        return Message.Text(gson.toJson(convertedData))
    }

    class Factory(
        private val gson: Gson
    ) : MessageAdapter.Factory {
        override fun create(type: Type, annotations: Array<Annotation>): MessageAdapter<*> {
            return CustomGsonMessageAdapter<Any>(gson)
        }
    }
}