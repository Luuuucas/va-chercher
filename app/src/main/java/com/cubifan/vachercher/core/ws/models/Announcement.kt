package com.cubifan.vachercher.core.ws.models

import com.cubifan.vachercher.core.ws.WsConstants.WS_ANNOUNCEMENT_MESSAGE

data class Announcement(
    val message: String,
    val timestamp: Long?,
    val announcementType: Int?
) : BaseModel(WS_ANNOUNCEMENT_MESSAGE)
