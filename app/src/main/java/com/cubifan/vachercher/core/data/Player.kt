package com.cubifan.vachercher.core.data

data class Player(
    val name: String,
    val score: Long,
    val position: Int,
)
