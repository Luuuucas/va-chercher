package com.cubifan.vachercher.core.api.response

data class RoomsResponse(
    val data: Array<RoomData>?,
    val message: String?,
    val successful: Boolean
)

data class RoomData(
    val name: String,
    val maxPlayers: Int,
    val playerCount: Int,
    val themeId: Int,
    val isPrivate: Boolean
)

data class RoomCreatedResponse(
    val data: RoomInitData?,
    val message: String?,
    val successful: Boolean
)

data class RoomInitData(
    val name: String,
    val maxPlayers: Int,
    val isPrivate: Boolean,
    val players: Array<Int>,
    val phase: String
)

sealed class Phase(val state: String) {
    object WAITING : Phase("WAITING_FOR_PLAYERS")
}
