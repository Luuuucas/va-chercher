package com.cubifan.vachercher.core.ws.models

import com.cubifan.vachercher.core.ws.WsConstants.WS_DISCONNECTION_REQUEST

data class DisconnectionRequest(
    val userUid: String,
    val isFinalDisconnection: Boolean = false
) : BaseModel(WS_DISCONNECTION_REQUEST)