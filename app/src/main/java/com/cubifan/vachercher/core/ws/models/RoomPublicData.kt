package com.cubifan.vachercher.core.ws.models

import com.cubifan.vachercher.core.ws.WsConstants.WS_ROOM_PUBLIC_DATA

data class RoomPublicData(
    val actualRound: Int,
    val numberOfRounds: Int,
    val numberOfConnectedPlayers: Int,
    val maxNumberOfPlayers: Int,
    val playersPublicData: List<PlayerPublicData>,
    val endOfGame: Boolean = false
) : BaseModel(WS_ROOM_PUBLIC_DATA)
