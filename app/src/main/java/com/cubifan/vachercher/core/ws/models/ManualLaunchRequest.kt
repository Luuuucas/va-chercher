package com.cubifan.vachercher.core.ws.models

import com.cubifan.vachercher.core.ws.WsConstants.WS_MANUAL_LAUNCH

data class ManualLaunchRequest(
    val roomName: String,
    val userUid: String
) : BaseModel(WS_MANUAL_LAUNCH)
