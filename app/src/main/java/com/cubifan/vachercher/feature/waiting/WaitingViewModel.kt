package com.cubifan.vachercher.feature.waiting

import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel

class WaitingViewModel : ViewModel() {
    val waitingForStart = mutableStateOf(false)
}