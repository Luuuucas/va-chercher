package com.cubifan.vachercher.feature.leaderboard

import android.annotation.SuppressLint
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateListOf
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.cubifan.vachercher.core.components.Leaderboard
import com.cubifan.vachercher.core.data.Player

@Composable
fun WorldLeaderboard(leaderboardPlayers: List<Player>) {
    Box(contentAlignment = Alignment.Center, modifier = Modifier.fillMaxSize()) {
        Text(
            text = "Classement mondial",
            fontSize = 28.sp,
            color = MaterialTheme.colors.primary,
            fontWeight = FontWeight.Bold,
            modifier = Modifier
                .align(Alignment.TopCenter)
                .padding(24.dp)
        )
        Leaderboard(
            header = listOf("Rang", "Joueur", "Score"),
            rows = leaderboardPlayers
        )
    }
}

@SuppressLint("UnrememberedMutableState")
@Preview
@Composable
private fun WorldLeaderboardPreview() {
    WorldLeaderboard(mutableStateListOf(Player("Lucas", 100, 1)))
}
