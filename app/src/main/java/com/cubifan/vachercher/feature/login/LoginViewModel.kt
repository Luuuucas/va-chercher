package com.cubifan.vachercher.feature.login

import android.app.Application
import android.content.Context
import androidx.compose.material.SnackbarHostState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.ui.text.input.TextFieldValue
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.viewModelScope
import com.cubifan.vachercher.BuildConfig
import com.cubifan.vachercher.R
import com.cubifan.vachercher.core.api.requests.UserRequest
import com.cubifan.vachercher.core.di.DependencyInjector
import com.cubifan.vachercher.utils.LocaleUtils
import com.cubifan.vachercher.utils.UserInfos
import com.cubifan.vachercher.utils.showNewSnackbar
import com.google.firebase.FirebaseNetworkException
import com.google.firebase.FirebaseTooManyRequestsException
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import kotlinx.coroutines.launch

class LoginViewModel(application: Application) : AndroidViewModel(application) {
    val login = mutableStateOf(TextFieldValue())
    val password = mutableStateOf(TextFieldValue())
    val logged = mutableStateOf(false)
    val snackbarHostState = mutableStateOf(SnackbarHostState())

    fun signIn(hapticAction: () -> Unit) {

        try {
            Firebase.auth.signInWithEmailAndPassword(login.value.text, password.value.text)
                .addOnCompleteListener {
                    val tag = "Sign in with email and password"
                    if (it.isSuccessful) {
                        loginToApi(hapticAction)
                    } else {
                        viewModelScope.launch {
                            when (it.exception) {
                                is FirebaseNetworkException -> snackbarHostState.value.showNewSnackbar(
                                    message = "Vérifiez votre connexion",
                                    hapticAction = hapticAction
                                )
                                is FirebaseTooManyRequestsException -> snackbarHostState.value.showNewSnackbar(
                                    message = "Requête bloquée suite à une activité inhabituelle. Veuillez réessayer plus tard.",
                                    hapticAction = hapticAction
                                )
                                else -> snackbarHostState.value.showNewSnackbar(
                                    message = "Connexion échouée. Vérifiez les informations transmises.",
                                    hapticAction = hapticAction
                                )
                            }
                        }
                    }
                }
        } catch (e: Exception) {
            viewModelScope.launch {
                snackbarHostState.value.showNewSnackbar(
                    "Veuillez remplir les champs correctement",
                    hapticAction
                )
            }
        }
    }

    private fun loginToApi(hapticAction: () -> Unit) {
        viewModelScope.launch {
            val loginResponse =
                DependencyInjector.API_SERVICE.login(
                    UserRequest(
                        uid = UserInfos.uid
                    )
                )

            loginResponse.body()?.let { response ->
                if (response.successful) {
                    Firebase.auth.currentUser?.getIdToken(true)?.addOnSuccessListener { task ->
                        getApplication<Application>().getSharedPreferences(
                            getApplication<Application>().getString(
                                R.string.firebase_token_shared_preferences,
                                BuildConfig.APPLICATION_ID
                            ),
                            Context.MODE_PRIVATE
                        ).edit().putString(LocaleUtils.FIREBASE_TOKEN, task.token ?: "").apply()

                        Firebase.auth.currentUser?.uid?.let {
                            UserInfos.uid = it
                        }

                        logged.value = true
                    }
                } else {
                    snackbarHostState.value.showNewSnackbar(
                        response.message!!,
                        hapticAction
                    )
                }
            } ?: run {
                snackbarHostState.value.showNewSnackbar(
                    "Une erreur inattendue s'est produite. Veuillez réessayer.",
                    hapticAction
                )
            }
        }
    }
}