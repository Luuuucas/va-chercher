package com.cubifan.vachercher.feature.badge

import androidx.annotation.DrawableRes
import androidx.compose.animation.core.Spring
import androidx.compose.animation.core.animateIntOffsetAsState
import androidx.compose.animation.core.spring
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.horizontalScroll
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.rememberScrollState
import androidx.compose.material.LinearProgressIndicator
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.graphics.ColorMatrix
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.IntOffset
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.viewmodel.compose.viewModel
import com.cubifan.vachercher.R
import com.cubifan.vachercher.ui.theme.Brown
import com.cubifan.vachercher.utils.UserInfos

@Composable
fun BadgesView(viewModel: BadgesViewModel = viewModel()) {
    var isAnim by remember { mutableStateOf(false) }

    LaunchedEffect(Unit) {
        isAnim = true
    }

    val playOffset = animateIntOffsetAsState(
        targetValue = if (isAnim) IntOffset(x = 0, y = 0)
        else IntOffset(x = -800, y = 0),
        animationSpec = spring(dampingRatio = .64f, stiffness = Spring.StiffnessLow)
    )

    Column(
        modifier = Modifier.fillMaxSize(),
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.spacedBy(12.dp)
    ) {
        Text(
            text = "Badges",
            fontSize = 28.sp,
            color = MaterialTheme.colors.primary,
            fontWeight = FontWeight.Bold,
            modifier = Modifier
                .align(Alignment.CenterHorizontally)
                .padding(24.dp)
        )

        if (UserInfos.level != 0) {
            Image(
                painter = painterResource(id = drawableLevelBadge(UserInfos.level)),
                contentDescription = null,
                modifier = Modifier.padding(20.dp)
            )
        }
        for (typeBadge in 0..2) {
            val badgeTypeLabel = when (typeBadge) {
                0 -> "Niveau"
                1 -> "Score"
                else -> "Victoires"
            }

            Text(
                text = badgeTypeLabel,
                textAlign = TextAlign.Start,
                fontWeight = FontWeight.Bold,
                fontSize = 24.sp,
                modifier = Modifier.padding(top = 24.dp)
            )
            Row(
                Modifier
                    .horizontalScroll(rememberScrollState())
                    .background(MaterialTheme.colors.primary)
                    .padding(horizontal = 4.dp)
                    .offset { playOffset.value }
            ) {
                for (badge in 0 until viewModel.obtainedBadges[typeBadge].second) {
                    BadgeObject(
                        isAchieved = achieved(typeBadge) > badge,
                        progressVisible = achieved(typeBadge) == badge,
                        badge = drawableBadge(
                            typeBadge,
                            if (typeBadge == 0) badge + 1 else badge
                        )
                    )
                }
            }
        }
    }
}

private fun achieved(typeBadge: Int): Int =
    when (typeBadge) {
        0 -> UserInfos.level
        1 -> {
            UserInfos.score.let {
                if (it < 50) 0
                else if (it < 100) 1
                else if (it < 200) 2
                else if (it < 500) 3
                else if (it < 1000) 4
                else if (it < 5000) 5
                else if (it < 7000) 6
                else 7
            }
        }
        else -> {
            UserInfos.wins.let {
                if (it < 5) 0
                else if (it < 10) 1
                else if (it < 20) 2
                else if (it < 50) 3
                else if (it < 100) 4
                else if (it < 200) 5
                else if (it < 500) 6
                else if (it < 700) 7
                else 8
            }
        }
    }

private fun drawableBadge(type: Int, number: Int): Int =
    when (type) {
        0 -> drawableLevelBadge(number)
        1 -> drawableScoreBadge(number)
        else -> drawableWinBadge(number)
    }

private fun drawableLevelBadge(number: Int): Int =
    when (number) {
        9 -> R.drawable.level9
        8 -> R.drawable.level8
        7 -> R.drawable.level7
        6 -> R.drawable.level6
        5 -> R.drawable.level5
        4 -> R.drawable.level4
        3 -> R.drawable.level3
        2 -> R.drawable.level2
        else -> R.drawable.level1
    }

private fun drawableScoreBadge(number: Int): Int =
    when (number) {
        0 -> R.drawable.score50
        1 -> R.drawable.score100
        2 -> R.drawable.score200
        3 -> R.drawable.score500
        5 -> R.drawable.score2000
        6 -> R.drawable.score5000
        4 -> R.drawable.score1000
        7 -> R.drawable.score7000
        else -> R.drawable.score10000
    }

private fun drawableWinBadge(number: Int): Int =
    when (number) {
        0 -> R.drawable.win1
        1 -> R.drawable.win2
        2 -> R.drawable.win3
        3 -> R.drawable.win4
        4 -> R.drawable.win5
        5 -> R.drawable.win6
        6 -> R.drawable.win7
        7 -> R.drawable.win8
        else -> R.drawable.win9
    }

@Composable
fun BadgeObject(isAchieved: Boolean, progressVisible: Boolean, @DrawableRes badge: Int) {
    Box(contentAlignment = Alignment.BottomCenter) {
        val filter =
            if (isAchieved) null else ColorFilter.colorMatrix(ColorMatrix().apply {
                setToSaturation(0f)
            })
        Image(
            painter = painterResource(badge),
            contentDescription = null,
            colorFilter = filter,
            modifier = Modifier
                .padding(20.dp)
                .size(50.dp, 50.dp)
        )
        if (progressVisible) LinearProgressIndicator(
            progress = .2f,
            modifier = Modifier
                .width(50.dp)
                .offset(y = (-10).dp),
            color = Brown
        )
    }
}

@Preview
@Composable
fun BadgeObjectPreview() {
    Column {
        BadgeObject(false, true, R.drawable.score100)
        BadgeObject(true, false, R.drawable.score1000)
    }
}

@Preview
@Composable
fun BadgePreview() {
    BadgesView()
}