package com.cubifan.vachercher.feature.registration

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.hapticfeedback.HapticFeedbackType
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalFocusManager
import androidx.compose.ui.platform.LocalHapticFeedback
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardCapitalization
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.viewmodel.compose.viewModel
import com.cubifan.vachercher.R
import com.cubifan.vachercher.core.components.Credentials
import com.cubifan.vachercher.core.components.CustomTextField
import com.cubifan.vachercher.core.components.DownRoundButton
import com.cubifan.vachercher.core.components.MiniSpacer
import com.cubifan.vachercher.ui.theme.DeepOrange
import com.cubifan.vachercher.ui.theme.Orange
import com.cubifan.vachercher.ui.theme.VaChercherTheme

@Composable
fun RegistrationView(navUserCreated: () -> Unit, viewModel: RegistrationViewModel = viewModel()) {
    val context = LocalContext.current
    val focusManager = LocalFocusManager.current
    val haptic = LocalHapticFeedback.current
    if (viewModel.userCreated.value) navUserCreated(); viewModel.userCreated.value = false

    Box(
        Modifier
            .fillMaxSize()
            .background(Orange)
    ) {
        Column(
            horizontalAlignment = Alignment.CenterHorizontally,
            modifier = Modifier
                .padding(30.dp)
                .verticalScroll(rememberScrollState())
        ) {
            Text(
                text = context.getString(R.string.account_creation),
                fontSize = 40.sp,
                textAlign = TextAlign.Center,
                color = Color.White,
                modifier = Modifier.padding(top = 100.dp)
            )
            Spacer(Modifier.height(50.dp))
            CustomTextField(
                label = "Pseudo",
                placeholder = "Pseudo123",
                textFieldData = viewModel.pseudo,
                maxSize = 12,
                keyboardOptions = KeyboardOptions.Default.copy(
                    capitalization = KeyboardCapitalization.Words,
                    autoCorrect = false,
                    keyboardType = KeyboardType.Text,
                    imeAction = ImeAction.Next,
                )
            )
            Spacer(Modifier.height(50.dp))
            Credentials(
                login = viewModel.login,
                password = viewModel.password,
                snackbarHostState = viewModel.snackbarHostState.value,
                passwordAction = ImeAction.Next,
                focusManager = focusManager
            )
            MiniSpacer()
            CustomTextField(
                label = "Confirmer",
                placeholder = "Confirmation du mot de passe",
                textFieldData = viewModel.confirmPassword,
                keyboardOptions = KeyboardOptions.Default.copy(
                    capitalization = KeyboardCapitalization.None,
                    autoCorrect = false,
                    keyboardType = KeyboardType.Password,
                    imeAction = ImeAction.Done,
                )
            )
            Spacer(Modifier.height(50.dp))
            DownRoundButton(
                onClick = { viewModel.createUser { haptic.performHapticFeedback(HapticFeedbackType.LongPress) } },
                text = context.getString(R.string.create_account),
                color = DeepOrange
            )
        }
    }
}

@Preview(showBackground = true)
@Composable
fun RegistrationPreview() {
    VaChercherTheme {
        RegistrationView({})
    }
}