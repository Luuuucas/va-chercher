package com.cubifan.vachercher.feature.room

import android.app.Application
import androidx.compose.animation.core.*
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.lifecycle.SavedStateHandle
import com.cubifan.vachercher.MainViewModel
import com.cubifan.vachercher.R
import com.cubifan.vachercher.core.components.CustomPopup
import com.cubifan.vachercher.core.ws.models.JoinRoomHandshake
import com.cubifan.vachercher.core.ws.models.JoinRoomHandshakeResponse
import com.cubifan.vachercher.ui.theme.Orange
import com.cubifan.vachercher.ui.theme.VaChercherTheme
import com.cubifan.vachercher.utils.UserInfos
import kotlinx.coroutines.launch

@Composable
fun RoomReconnectionView(
    mainViewModel: MainViewModel,
    roomName: String,
    joinGame: (String) -> Unit,
    quitGame: () -> Unit
) {

    LaunchedEffect(Unit) {
        launch {
            mainViewModel.sendBaseModel(
                JoinRoomHandshake(
                    UserInfos.pseudo, roomName, UserInfos.uid
                )
            )
        }
        launch {
            mainViewModel.joinRoomHandshakeResponse.collect {
                if (it.isSuccessful) {
                    joinGame(roomName)
                    mainViewModel.joinRoomHandshakeResponse.value = JoinRoomHandshakeResponse(false)
                }
            }
        }
    }

    Column(
        verticalArrangement = Arrangement.SpaceAround,
        horizontalAlignment = Alignment.CenterHorizontally,
        modifier = Modifier
            .fillMaxSize()
            .background(Orange)
    ) {
        val infiniteTransition = rememberInfiniteTransition()
        val alpha = infiniteTransition.animateFloat(
            initialValue = 1f, targetValue = 0f, animationSpec = infiniteRepeatable(
                tween(1000, easing = LinearEasing), RepeatMode.Reverse
            )
        )
        Image(
            painter = painterResource(id = R.drawable.ic_launcher),
            contentDescription = null,
            alpha = alpha.value,
            modifier = Modifier.fillMaxSize(.8f)
        )

    }

    CustomPopup(showing = mainViewModel.joinRoomHandshakeErrorReceived,
        allowDismissRequest = false,
        titleText = stringResource(R.string.join_room_error_title),
        textText = mainViewModel.joinRoomHandshakeErrorMessage.value,
        confirmButtonText = "Retour à l'accueil",
        confirmButtonAction = {
            quitGame()

            mainViewModel.joinRoomHandshakeErrorReceived.value = false
            mainViewModel.joinRoomHandshakeErrorMessage.value = ""
        },
        dismissButtonAction = {})
}


@Preview(showBackground = true)
@Composable
fun RoomReconnectionPreview() {
    VaChercherTheme {
        RoomReconnectionView(
            mainViewModel = MainViewModel(
                application = Application(),
                savedStateHandle = SavedStateHandle()
            ),
            roomName = "Room",
            joinGame = {},
            quitGame = {}
        )
    }
}