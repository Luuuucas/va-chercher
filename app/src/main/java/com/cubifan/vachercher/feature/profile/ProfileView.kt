package com.cubifan.vachercher.feature.profile

import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.animation.scaleIn
import androidx.compose.animation.scaleOut
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Star
import androidx.compose.material.icons.filled.Warning
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.cubifan.vachercher.R
import com.cubifan.vachercher.core.components.UserDisplay
import com.cubifan.vachercher.ui.theme.Error
import com.cubifan.vachercher.utils.UserInfos

@OptIn(ExperimentalAnimationApi::class)
@Composable
fun ProfileView(logout: () -> Unit, deleteUser: () -> Unit) {
    var userWantsToDeleteAccount by remember { mutableStateOf(false) }
    val anim = remember { mutableStateOf(false) }
    if (userWantsToDeleteAccount) {
        AlertDialog(
            onDismissRequest = { userWantsToDeleteAccount = false },
            confirmButton = {
                Button(onClick = { deleteUser(); userWantsToDeleteAccount = false }
                ) {
                    Text("Oui")
                }
            },
            dismissButton = {
                Button(onClick = { userWantsToDeleteAccount = false }) {
                    Text("Non")
                }
            },
            title = {
                Row(
                    verticalAlignment = Alignment.CenterVertically,
                    horizontalArrangement = Arrangement.spacedBy(8.dp)
                ) {
                    Icon(
                        imageVector = Icons.Default.Warning,
                        tint = Error,
                        contentDescription = null
                    )
                    Text("Es-tu sur ?")
                }
            },
            text = { Text("Tu ne pourras plus utiliser ce compte") }
        )
    }
    LaunchedEffect(anim) {
        anim.value = true
    }
    Column(
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.SpaceBetween,
        modifier = Modifier.fillMaxSize(),
    ) {
        UserDisplay(
            pseudo = UserInfos.pseudo,
            level = UserInfos.level
        )

        AnimatedVisibility(
            enter = scaleIn(),
            exit = scaleOut(),
            visible = anim.value
        ) {
            CustomSuccess(title = "Score global", points = UserInfos.score)
        }
        AnimatedVisibility(
            enter = scaleIn(),
            exit = scaleOut(),
            visible = anim.value
        ) {
            CustomSuccess(title = "Victoires", points = UserInfos.wins)
        }

        Button(
            onClick = logout,
            contentPadding = PaddingValues(vertical = 24.dp),
            shape = RoundedCornerShape(topEnd = 32.dp, bottomEnd = 32.dp),
            modifier = Modifier
                .align(Alignment.Start)
                .fillMaxWidth(.85f)
        ) {
            Text(stringResource(R.string.logout), fontSize = 24.sp, color = Color.White)
        }

        Button(
            onClick = { userWantsToDeleteAccount = true },
            contentPadding = PaddingValues(vertical = 24.dp),
            colors = ButtonDefaults.buttonColors(backgroundColor = Error),
            shape = RoundedCornerShape(topEnd = 32.dp, bottomEnd = 32.dp),
            modifier = Modifier
                .align(Alignment.Start)
                .fillMaxWidth(.7f)
                .padding(bottom = 8.dp)
        ) {
            Text("Supprimer le compte", fontSize = 18.sp, color = Color.White)
        }
    }
}

@Composable
private fun CustomSuccess(title: String, points: Int) {
    Box(Modifier.clip(RoundedCornerShape(8.dp))) {
        Column(
            modifier = Modifier
                .background(MaterialTheme.colors.primary)
                .fillMaxWidth(.7f)
                .padding(vertical = 24.dp),
            horizontalAlignment = Alignment.CenterHorizontally,
            verticalArrangement = Arrangement.spacedBy(16.dp)
        ) {
            Text(
                text = title,
                fontWeight = FontWeight.SemiBold,
                fontSize = 20.sp,
            )
            Icon(
                modifier = Modifier.size(50.dp),
                imageVector = Icons.Default.Star,
                contentDescription = null,
                tint = Color.Yellow
            )
            Text(
                points.toString(),
                color = Color.White,
                fontWeight = FontWeight.Bold,
                fontSize = 32.sp
            )
        }
    }
}

@Preview
@Composable
fun ProfilePreview() {
    ProfileView({}, {})
}
