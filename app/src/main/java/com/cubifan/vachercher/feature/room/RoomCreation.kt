package com.cubifan.vachercher.feature.room

import android.annotation.SuppressLint
import android.app.Application
import android.media.MediaPlayer
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Add
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.hapticfeedback.HapticFeedbackType
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalHapticFeedback
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardCapitalization
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.viewmodel.compose.viewModel
import com.cubifan.vachercher.MainViewModel
import com.cubifan.vachercher.R
import com.cubifan.vachercher.core.components.CustomPopup
import com.cubifan.vachercher.core.components.CustomTextField
import com.cubifan.vachercher.core.components.DownRoundButton
import com.cubifan.vachercher.core.components.LittleSpacer
import com.cubifan.vachercher.core.ws.models.JoinRoomHandshake
import com.cubifan.vachercher.core.ws.models.JoinRoomHandshakeResponse
import com.cubifan.vachercher.ui.theme.Error
import com.cubifan.vachercher.ui.theme.VaChercherTheme
import com.cubifan.vachercher.utils.UserInfos
import kotlinx.coroutines.launch

enum class InputType {
    PERSONS, ROUNDS
}

@Composable
fun RoomCreationView(
    viewModel: RoomViewModel = viewModel(),
    mainViewModel: MainViewModel,
    createGame: (String) -> Unit = {}
) {
    val haptic = LocalHapticFeedback.current

    LaunchedEffect("RoomCreation") {
        launch {
            viewModel.roomCreationState.collect { roomCreationState ->
                when (roomCreationState) {
                    is RoomViewModel.RoomCreationState.Success -> {
                        mainViewModel.sendBaseModel(
                            JoinRoomHandshake(
                                UserInfos.pseudo,
                                roomCreationState.roomName ?: "unknown",
                                UserInfos.uid
                            )
                        )
                    }
                    else -> {}
                }
            }
        }

        launch {
            mainViewModel.joinRoomHandshakeResponse.collect {
                if (it.isSuccessful) {
                    createGame(viewModel.roomCreationState.value.roomName ?: "")
                    mainViewModel.joinRoomHandshakeResponse.value = JoinRoomHandshakeResponse(false)
                }
            }
        }

        launch {
            viewModel.firebaseTokenIdExpired.collect {
                if (it) {
                    mainViewModel.firebaseTokenIdExpired.value = it
                    viewModel.firebaseTokenIdExpired.value = false
                }
            }
        }
    }

    LazyColumn(
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.SpaceAround,
        modifier = Modifier.padding(30.dp)
    ) {
        val isPrivateRoom = viewModel.isPrivateRoom
        item {
            CustomTextField(
                label = "Nom de la salle",
                placeholder = "Rejoins ma salle !",
                textFieldData = viewModel.roomName,
                keyboardOptions = KeyboardOptions.Default.copy(
                    capitalization = KeyboardCapitalization.Words,
                    autoCorrect = true,
                    imeAction = ImeAction.Next,
                )
            )
            LittleSpacer()
            NumberInput(viewModel.maxNumPlayers, InputType.PERSONS)
            LittleSpacer()
            NumberInput(viewModel.maxNumRounds, InputType.ROUNDS)
            LittleSpacer()

            /*
            Row(verticalAlignment = Alignment.CenterVertically) {
                Text("Salle privée")
                Switch(
                    checked = isPrivateRoom.value,
                    onCheckedChange = {
                        isPrivateRoom.value = it
                    }
                )
            }
             */
            if (isPrivateRoom.value) {
                CustomTextField(
                    label = "Mot de passe",
                    placeholder = "my_password_123",
                    textFieldData = viewModel.password,
                    keyboardOptions = KeyboardOptions.Default.copy(
                        capitalization = KeyboardCapitalization.Words,
                        autoCorrect = false,
                        imeAction = ImeAction.Done,
                    )
                )
            }
            LittleSpacer()
            DownRoundButton(
                onClick = { viewModel.createRoom { haptic.performHapticFeedback(HapticFeedbackType.LongPress) } },
                text = "Créer la salle !",
                fontSize = 24.sp
            )
        }
    }
    SnackbarHost(
        hostState = viewModel.snackbarHostState.value,
        snackbar = { Snackbar(it, backgroundColor = Error) }
    )

    CustomPopup(showing = mainViewModel.joinRoomHandshakeErrorReceived,
        allowDismissRequest = false,
        titleText = stringResource(R.string.join_room_error_title),
        textText = mainViewModel.joinRoomHandshakeErrorMessage.value,
        confirmButtonText = "Ok !",
        confirmButtonAction = {
            mainViewModel.joinRoomHandshakeErrorReceived.value = false

            mainViewModel.joinRoomHandshakeErrorMessage.value = ""
        },
        dismissButtonAction = {})
}

@Composable
private fun NumberInput(input: MutableState<Int>, type: InputType) {
    val (min, max) = type.ordinal + 1 to 15
    val clicSound = MediaPlayer.create(LocalContext.current, R.raw.clic)
    Surface(shape = RoundedCornerShape(8.dp), color = MaterialTheme.colors.primary) {
        Row(
            horizontalArrangement = Arrangement.SpaceBetween,
            verticalAlignment = Alignment.CenterVertically,
            modifier = Modifier
                .fillMaxWidth(.6f)
                .height(60.dp)
        ) {
            IconButton(onClick = { clicSound.start(); if (input.value > min) input.value-- }) {
                Icon(
                    painter = painterResource(id = R.drawable.remove_24),
                    contentDescription = "-",
                    tint = Color.White
                )
            }
            Row {
                Text(
                    text = input.value.let {
                        if (it == 1) "solo" else it.toString()
                    },
                    fontWeight = FontWeight.Bold
                )
                if (type == InputType.PERSONS) {
                    Icon(
                        painter = painterResource(R.drawable.person_white_24dp),
                        contentDescription = "Persons"
                    )
                } else {
                    Text(text = " rounds", fontWeight = FontWeight.Bold)
                }
            }
            IconButton(onClick = { clicSound.start(); if (input.value < max) input.value++ }) {
                Icon(Icons.Default.Add, contentDescription = "+", tint = Color.White)
            }
        }
    }
}

@SuppressLint("UnrememberedMutableState")
@Preview(showBackground = true)
@Composable
fun NumberInputPreview() {
    Box(
        Modifier
            .fillMaxWidth()
            .wrapContentWidth(Alignment.CenterHorizontally)
    ) {
        NumberInput(mutableStateOf(5), InputType.PERSONS)
    }
}

@Preview(showBackground = true)
@Composable
fun RoomCreationPreview() {
    VaChercherTheme {
        RoomCreationView(
            viewModel = RoomViewModel(
                application = Application(),
                savedStateHandle = SavedStateHandle()
            ),
            mainViewModel = MainViewModel(
                application = Application(),
                savedStateHandle = SavedStateHandle()
            ),
            createGame = {}
        )
    }
}