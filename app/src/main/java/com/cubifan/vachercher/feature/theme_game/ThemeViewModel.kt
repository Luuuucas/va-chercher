package com.cubifan.vachercher.feature.theme_game

import android.app.Application
import android.content.Context
import android.util.Log
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateListOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.viewModelScope
import com.cubifan.vachercher.BuildConfig
import com.cubifan.vachercher.R
import com.cubifan.vachercher.core.di.DependencyInjector
import com.cubifan.vachercher.feature.theme_game.data.Theme
import com.cubifan.vachercher.utils.LocaleUtils
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch
import java.net.HttpURLConnection

class ThemeViewModel(application: Application) : AndroidViewModel(application) {
    var apiCallInProgress by mutableStateOf(false)
    val themes = mutableStateListOf<Theme>()
    var firebaseTokenIdExpired = MutableStateFlow(false)


    init {
        getThemes()
    }

    fun getThemes() {
        apiCallInProgress = true
        viewModelScope.launch {
            try {
                val response = DependencyInjector.API_SERVICE.getThemes(
                    "Bearer " + getApplication<Application>().getSharedPreferences(
                        getApplication<Application>().getString(
                            R.string.firebase_token_shared_preferences, BuildConfig.APPLICATION_ID
                        ), Context.MODE_PRIVATE
                    ).getString(LocaleUtils.FIREBASE_TOKEN, "") as String
                )
                if (response.isSuccessful) {
                    themes.clear()
                    response.body()?.data?.forEach {
                        themes.add(
                            Theme(
                                it.id, it.name, it.description, it.urlPhoto, (it.levelRequired ?: 0)
                            )
                        )
                    }
                } else {
                    if (response.code() == HttpURLConnection.HTTP_UNAUTHORIZED) {
                        firebaseTokenIdExpired.value = true
                    }
                }
            } catch (e: Exception) {
                Log.d("API theme failure", e.message.toString())
            }
            apiCallInProgress = false
        }
    }
}