package com.cubifan.vachercher.feature.room

import android.app.Application
import android.content.Context
import android.util.Log
import androidx.compose.material.SnackbarHostState
import androidx.compose.runtime.mutableStateListOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.ui.text.input.TextFieldValue
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.viewModelScope
import com.cubifan.vachercher.BuildConfig
import com.cubifan.vachercher.NavPath
import com.cubifan.vachercher.R
import com.cubifan.vachercher.core.api.requests.RoomRequest
import com.cubifan.vachercher.core.data.Room
import com.cubifan.vachercher.core.di.DependencyInjector
import com.cubifan.vachercher.utils.LocaleUtils
import com.cubifan.vachercher.utils.showNewSnackbar
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.net.HttpURLConnection

class RoomViewModel(
    application: Application,
    savedStateHandle: SavedStateHandle
) : AndroidViewModel(application) {
    val roomName = mutableStateOf(TextFieldValue())
    var maxNumPlayers = mutableStateOf(5)
    var maxNumRounds = mutableStateOf(5)
    var isPrivateRoom = mutableStateOf(false)
    var password = mutableStateOf(TextFieldValue())

    val rooms = mutableStateListOf<Room>()
    val apiCallInProgress = mutableStateOf(false)

    private val themeId = savedStateHandle.get<String>(NavPath.Theme.argument)

    val snackbarHostState = mutableStateOf(SnackbarHostState())

    private val _roomCreationState = MutableStateFlow<RoomCreationState>(RoomCreationState.Empty())
    val roomCreationState: StateFlow<RoomCreationState> = _roomCreationState
    var firebaseTokenIdExpired = MutableStateFlow(false)

    val roomChosen = mutableStateOf("")

    sealed class RoomCreationState(val roomName: String?) {
        class Success(val room: String) : RoomCreationState(room)
        class Empty : RoomCreationState(null)
    }

    init {
        getRooms()
    }

    fun getRooms() {
        apiCallInProgress.value = true
        viewModelScope.launch {
            try {
                val apiResponse = DependencyInjector.API_SERVICE.getRoomsByTheme(
                    themeId!!,
                    "Bearer " + getApplication<Application>().getSharedPreferences(
                        getApplication<Application>().getString(
                            R.string.firebase_token_shared_preferences,
                            BuildConfig.APPLICATION_ID
                        ),
                        Context.MODE_PRIVATE
                    ).getString(LocaleUtils.FIREBASE_TOKEN, "") as String
                )

                apiResponse.body()?.let { response ->
                    if (response.successful) {
                        rooms.clear()
                        for (room in response.data!!) {
                            this@RoomViewModel.rooms += Room(
                                room.name,
                                room.isPrivate,
                                room.maxPlayers,
                                room.playerCount,
                                room.themeId
                            )
                        }
                    } else {
                        snackbarHostState.value.showSnackbar(response.message!!)
                    }
                } ?: run {
                    if (apiResponse.code() == HttpURLConnection.HTTP_UNAUTHORIZED) {
                        firebaseTokenIdExpired.value = true
                    }

                    snackbarHostState.value.showSnackbar("Une erreur inattendue s'est produite. Veuillez réessayer.")
                }
            } catch (e: Exception) {
                Log.e("GET_ROOMS", e.message.toString())
                //snackbarHostState.value.showSnackbar(e.message.toString())
            }
            withContext(Dispatchers.Main) {
                apiCallInProgress.value = false
            }
        }
    }

    fun createRoom(hapticAction: () -> Unit) {
        viewModelScope.launch {
            try {
                check(roomName.value.text.isNotBlank()) { "Le nom de la salle est vide" }
                val apiResponse =
                    DependencyInjector.API_SERVICE.createRoom(
                        RoomRequest(
                            name = roomName.value.text.trim(),
                            maxPlayers = maxNumPlayers.value,
                            numberOfRounds = maxNumRounds.value,
                            isPrivate = isPrivateRoom.value,
                            password = password.value.text,
                            themeId = themeId!!
                        ),
                        "Bearer " + getApplication<Application>().getSharedPreferences(
                            getApplication<Application>().getString(
                                R.string.firebase_token_shared_preferences,
                                BuildConfig.APPLICATION_ID
                            ),
                            Context.MODE_PRIVATE
                        ).getString(LocaleUtils.FIREBASE_TOKEN, "") as String
                    )

                apiResponse.body()?.let { response ->
                    if (response.successful) {
                        _roomCreationState.value =
                            RoomCreationState.Success(response.data!!.name)
                    } else {
                        snackbarHostState.value.showNewSnackbar(
                            response.message!!,
                            hapticAction
                        )
                    }
                } ?: run {
                    if (apiResponse.code() == HttpURLConnection.HTTP_UNAUTHORIZED) {
                        firebaseTokenIdExpired.value = true
                    }

                    snackbarHostState.value.currentSnackbarData?.dismiss()
                    snackbarHostState.value.showNewSnackbar(
                        "Une erreur inattendue s'est produite. Veuillez réessayer.",
                        hapticAction
                    )
                }
            } catch (e: Exception) { //TODO gérer exception back ET exception classique
                snackbarHostState.value.showNewSnackbar(
                    e.message.toString(),
                    hapticAction
                )
            }
        }
    }
}
