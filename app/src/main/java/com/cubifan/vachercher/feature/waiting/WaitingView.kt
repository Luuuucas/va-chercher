package com.cubifan.vachercher.feature.waiting

import androidx.activity.compose.BackHandler
import androidx.compose.animation.core.*
import androidx.compose.foundation.Canvas
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.StrokeCap
import androidx.compose.ui.graphics.drawscope.rotate
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.viewmodel.compose.viewModel
import com.cubifan.vachercher.MainViewModel
import com.cubifan.vachercher.R
import com.cubifan.vachercher.core.components.CustomPopup
import com.cubifan.vachercher.core.ws.models.ManualLaunchRequest
import com.cubifan.vachercher.core.ws.models.Room
import com.cubifan.vachercher.ui.theme.DeepOrange
import com.cubifan.vachercher.ui.theme.Orange
import com.cubifan.vachercher.utils.UserInfos
import kotlinx.coroutines.launch

@Composable
fun WaitingView(
    beginGame: () -> Unit,
    showResults: () -> Unit,
    isAdmin: Boolean,
    quitGame: () -> Unit = {},
    viewModel: WaitingViewModel = viewModel(),
    mainViewModel: MainViewModel,
    roomName: String
) {
    BackHandler(true) { mainViewModel.popupShowing.value = true }

    CustomPopup(showing = mainViewModel.popupShowing,
        allowDismissRequest = true,
        titleText = "Quitter ?",
        textText = "Es-tu sûr de quitter la partie?",
        confirmButtonText = "Oui",
        dismissButtonText = "Non",
        confirmButtonAction = quitGame,
        dismissButtonAction = {})
    //LockScreenOrientation(true)

    LaunchedEffect("Websocket") {
        mainViewModel.snackbarHostState.value.currentSnackbarData?.dismiss()
        mainViewModel.timerJob?.cancel()

        mainViewModel.roomName = roomName
        mainViewModel.collectSocketEvents()
        mainViewModel.collectConnectionEvents()

        launch {
            mainViewModel.currentGamePhase.collect {
                when (it.phase) {
                    Room.GamePhase.WAITING_FOR_START -> {
                        viewModel.waitingForStart.value = true
                    }
                    Room.GamePhase.SEARCH -> {
                        beginGame()
                    }
                    Room.GamePhase.SHOW_RESULTS -> {
                        showResults()
                    }
                    else -> Unit
                }
            }
        }
        launch {
            mainViewModel.gotKicked.collect {
                if (it) quitGame()
            }
        }
    }

    Column(
        verticalArrangement = Arrangement.SpaceAround,
        horizontalAlignment = Alignment.CenterHorizontally,
        modifier = Modifier
            .fillMaxSize()
            .background(if (viewModel.waitingForStart.value) Orange else Color.White)
    ) {
        if (viewModel.waitingForStart.value) {
            val infiniteTransition = rememberInfiniteTransition()
            val alpha = infiniteTransition.animateFloat(
                initialValue = 1f, targetValue = 0f, animationSpec = infiniteRepeatable(
                    tween(1000, easing = LinearEasing), RepeatMode.Reverse
                )
            )
            Image(
                painter = painterResource(id = R.drawable.ic_launcher),
                contentDescription = null,
                alpha = alpha.value,
                modifier = Modifier.fillMaxSize(.8f)
            )
        } else {
            Text(
                text = stringResource(if (mainViewModel.roomPublicDataState.value?.maxNumberOfPlayers == 1) R.string.launch_when_you_want else R.string.waiting_for_players),
                fontSize = 40.sp,
                textAlign = TextAlign.Center,
                modifier = Modifier
                    .width(IntrinsicSize.Max)
                    .align(Alignment.CenterHorizontally)
                    .padding(20.dp)
            )
            mainViewModel.roomPublicDataState.value?.maxNumberOfPlayers?.let {
                if (it > 1) {
                    CircularProgressIndicator()
                    PlayersConnected(
                        mainViewModel.roomPublicDataState.value?.maxNumberOfPlayers ?: 0,
                        mainViewModel.roomPublicDataState.value?.numberOfConnectedPlayers ?: 0
                    )
                }
            }
        }

        mainViewModel.roomPublicDataState.value?.maxNumberOfPlayers?.let {
            if (it > 1) {
                Divider(
                    color = MaterialTheme.colors.primary,
                    thickness = 8.dp,
                    modifier = Modifier
                        .fillMaxWidth(.8f)
                        .clip(CircleShape)
                )
            }
        }

        if (isAdmin && !viewModel.waitingForStart.value) {
            Button(
                onClick = {
                    mainViewModel.sendBaseModel(
                        ManualLaunchRequest(
                            roomName = roomName, userUid = UserInfos.uid
                        )
                    )
                },
                contentPadding = PaddingValues(vertical = 24.dp),
                modifier = Modifier
                    .align(Alignment.Start)
                    .fillMaxWidth()
            ) {
                Text(
                    "Jouer maintenant",
                    color = Color.White,
                    fontWeight = FontWeight.SemiBold,
                    fontSize = 28.sp
                )
            }
        }
    }

    SnackbarHost(
        hostState = mainViewModel.snackbarHostState.value,
        snackbar = { Snackbar(it, backgroundColor = DeepOrange) }
    )
}

@Composable
private fun PlayersConnected(maxPlayers: Int, connectedPlayers: Int) {
    Box(
        Modifier
            .width(150.dp)
            .height(150.dp)
    ) {
        val lineColor = MaterialTheme.colors.onSurface
        Text(
            text = connectedPlayers.toString(),
            fontSize = 55.sp,
            modifier = Modifier.align(Alignment.TopStart)
        )
        Canvas(Modifier.fillMaxSize()) {
            rotate(130f, Offset(center.x, center.y)) {
                drawLine(
                    color = lineColor,
                    start = Offset(0f, center.y),
                    end = Offset(size.width, center.y),
                    strokeWidth = 10f,
                    cap = StrokeCap.Round
                )
            }
        }
        Text(
            text = maxPlayers.toString(),
            fontSize = 55.sp,
            modifier = Modifier.align(Alignment.BottomEnd)
        )
    }
}

@Preview(showBackground = true)
@Composable
fun PlayersConnectedPreview() {
    PlayersConnected(15, viewModel())
}

@Preview(showSystemUi = true)
@Composable
fun WaitingPreview() {
    WaitingView({}, {}, false, mainViewModel = viewModel(), roomName = "toto")
}

@Preview(showSystemUi = true)
@Composable
fun WaitingAdminPreview() {
    WaitingView({}, {}, true, mainViewModel = viewModel(), roomName = "toto")
}