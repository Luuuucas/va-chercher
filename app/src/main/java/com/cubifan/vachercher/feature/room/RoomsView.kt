package com.cubifan.vachercher.feature.room

import android.Manifest
import android.app.Application
import android.content.Intent
import android.media.MediaPlayer
import android.net.Uri
import android.provider.Settings
import androidx.compose.foundation.*
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Add
import androidx.compose.material.icons.filled.Warning
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.viewmodel.compose.viewModel
import com.cubifan.vachercher.MainViewModel
import com.cubifan.vachercher.Permission
import com.cubifan.vachercher.R
import com.cubifan.vachercher.core.components.CustomPopup
import com.cubifan.vachercher.core.ws.models.JoinRoomHandshake
import com.cubifan.vachercher.core.ws.models.JoinRoomHandshakeResponse
import com.cubifan.vachercher.ui.theme.Error
import com.cubifan.vachercher.ui.theme.VaChercherTheme
import com.cubifan.vachercher.utils.UserInfos
import com.google.accompanist.permissions.ExperimentalPermissionsApi
import com.google.accompanist.swiperefresh.SwipeRefresh
import com.google.accompanist.swiperefresh.rememberSwipeRefreshState
import kotlinx.coroutines.launch

@OptIn(ExperimentalPermissionsApi::class)
@Composable
fun RoomsView(
    themeId: String?,
    viewModel: RoomViewModel = viewModel(),
    mainViewModel: MainViewModel,
    enterRoom: (String) -> Unit,
    createRoom: () -> Unit,
) {
    val context = LocalContext.current
    val clicSound = MediaPlayer.create(context, R.raw.clic)
    val rooms = viewModel.rooms

    LaunchedEffect("Websockets") {
        mainViewModel.collectSocketEvents()
        mainViewModel.collectConnectionEvents()

        launch {
            mainViewModel.joinRoomHandshakeResponse.collect {
                if (it.isSuccessful) {
                    enterRoom(viewModel.roomChosen.value)
                    viewModel.roomChosen.value = ""
                    mainViewModel.joinRoomHandshakeResponse.value = JoinRoomHandshakeResponse(false)
                }
            }
        }

        launch {
            viewModel.firebaseTokenIdExpired.collect {
                if (it) {
                    mainViewModel.firebaseTokenIdExpired.value = it
                    viewModel.firebaseTokenIdExpired.value = false
                }
            }
        }
    }

    Permission(
        permission = Manifest.permission.CAMERA,
        rationale = "Utilisez l'appareil photo pour scanner des objets et gagner des points !",
        permissionNotAvailableContent = {
            Surface(
                modifier = Modifier
                    .fillMaxSize()
                    .padding(30.dp)
            ) {
                Column(
                    horizontalAlignment = Alignment.CenterHorizontally,
                    modifier = Modifier.fillMaxSize(.8f)
                ) {
                    Icon(
                        imageVector = Icons.Default.Warning,
                        contentDescription = null,
                        tint = Color(0xFFEDE04D),
                        modifier = Modifier
                            .padding(24.dp)
                            .size(64.dp),
                    )
                    Text(
                        text = stringResource(id = R.string.no_camera_permission),
                        modifier = Modifier.fillMaxWidth(.9f),
                        textAlign = TextAlign.Justify
                    )
                    Spacer(Modifier.weight(1f))
                    Image(
                        painter = painterResource(id = R.drawable.waiting),
                        contentDescription = null,
                        modifier = Modifier.size(256.dp),
                    )
                    Spacer(Modifier.weight(1f))
                    Button(
                        onClick = {
                            clicSound.start()
                            context.startActivity(
                                Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS).apply {
                                    data = Uri.fromParts("package", context.packageName, null)
                                }
                            )
                        }
                    ) {
                        Text("Ouvrir les paramètres")
                    }
                }
            }
        }
    ) {
        Box(Modifier.fillMaxSize()) {
            SwipeRefresh(
                state = rememberSwipeRefreshState(isRefreshing = viewModel.apiCallInProgress.value),
                onRefresh = viewModel::getRooms,
            ) {
                if (viewModel.apiCallInProgress.value) {
                    Column(
                        Modifier
                            .fillMaxSize()
                            .verticalScroll(rememberScrollState())
                    ) {
                        LinearProgressIndicator(
                            Modifier.fillMaxWidth(),
                            color = MaterialTheme.colors.secondary
                        )
                    }
                } else {
                    if (rooms.isEmpty()) {
                        Column(
                            horizontalAlignment = Alignment.CenterHorizontally,
                            verticalArrangement = Arrangement.Center,
                            modifier = Modifier
                                .fillMaxSize()
                                .verticalScroll(rememberScrollState())
                        ) {
                            Image(
                                painter = painterResource(id = R.drawable.waiting),
                                contentDescription = null,
                                modifier = Modifier.fillMaxSize(.5f)
                            )
                            Text(context.getString(R.string.no_current_rooms))
                        }
                    } else {
                        Box(Modifier.padding(bottom = 50.dp)) {
                            LazyColumn(
                                verticalArrangement = Arrangement.spacedBy(16.dp),
                                horizontalAlignment = Alignment.CenterHorizontally,
                                modifier = Modifier
                                    .fillMaxSize()
                                    .padding(top = 16.dp)
                            ) {
                                item {
                                    for (room in rooms) {
                                        RoomCard(
                                            room.name,
                                            room.isPrivate,
                                            room.connectedPlayers,
                                            room.maxPlayers,
                                            viewModel,
                                            mainViewModel
                                        )
                                        Spacer(Modifier.height(8.dp))
                                    }
                                }
                            }
                        }
                    }
                }
            }
            ExtendedFloatingActionButton(
                onClick = { createRoom(); clicSound.start() },
                modifier = Modifier
                    .align(Alignment.BottomEnd)
                    .padding(30.dp),
                backgroundColor = MaterialTheme.colors.primary,
                text = {
                    Text(
                        "Créer une salle de jeu",
                        color = Color.White,
                        fontSize = 14.sp
                    )
                },
                icon = {
                    Icon(
                        Icons.Default.Add,
                        "+",
                        tint = Color.White,
                        modifier = Modifier.size(32.dp)
                    )
                }
            )
        }
    }

    SnackbarHost(
        hostState = viewModel.snackbarHostState.value,
        snackbar = { Snackbar(it, backgroundColor = Error) }
    )

    CustomPopup(showing = mainViewModel.joinRoomHandshakeErrorReceived,
        allowDismissRequest = false,
        titleText = stringResource(R.string.join_room_error_title),
        textText = mainViewModel.joinRoomHandshakeErrorMessage.value,
        confirmButtonText = "Ok !",
        confirmButtonAction = {
            viewModel.getRooms()
            mainViewModel.joinRoomHandshakeErrorReceived.value = false
            mainViewModel.joinRoomHandshakeErrorMessage.value = ""
        },
        dismissButtonAction = {})
}

@Composable
fun RoomCard(
    name: String,
    isPrivate: Boolean,
    numPlayers: Int,
    maxPlayers: Int,
    viewModel: RoomViewModel,
    mainViewModel: MainViewModel
) {
    Surface(
        modifier = Modifier
            .clickable {
                viewModel.roomChosen.value = name

                mainViewModel.sendBaseModel(
                    JoinRoomHandshake(
                        UserInfos.pseudo, name, UserInfos.uid
                    )
                )
            }
            .padding(horizontal = 32.dp),
        color = MaterialTheme.colors.primarySurface,
        border = BorderStroke(
            1.dp,
            Brush.horizontalGradient(listOf(Color.Black, Color.Gray))
        ),
        shape = RoundedCornerShape(8.dp),
        elevation = 8.dp
    ) {
        Row(modifier = Modifier.padding(16.dp), verticalAlignment = Alignment.CenterVertically) {
            Text(text = name, modifier = Modifier.weight(7f))
            /*
            if (isPrivate)
                Icon(
                    painter = painterResource(id = R.drawable.lock),
                    contentDescription = "private",
                    modifier = Modifier.weight(1f)
                )
            else
                Icon(
                    painter = painterResource(id = R.drawable.lock_open_variant),
                    contentDescription = "public",
                    modifier = Modifier.weight(1f)
                )
             */
            Text(
                text = "$numPlayers/$maxPlayers",
                textAlign = TextAlign.Center,
                modifier = Modifier.weight(2f)
            )
        }
    }
}

@Preview(showBackground = true)
@Composable
fun RoomPreview() {
    Column(
        verticalArrangement = Arrangement.spacedBy(16.dp),
        modifier = Modifier.padding(top = 16.dp),
    ) {
        val clicSound = MediaPlayer.create(LocalContext.current, R.raw.clic)
        RoomCard(
            "A very long name for a room because it cannot be displayed in one line",
            false,
            2,
            5,
            RoomViewModel(savedStateHandle = SavedStateHandle(), application = Application()),
            MainViewModel(savedStateHandle = SavedStateHandle(), application = Application())
        )
        RoomCard(
            "MyRoom",
            true,
            3,
            10,
            RoomViewModel(savedStateHandle = SavedStateHandle(), application = Application()),
            MainViewModel(savedStateHandle = SavedStateHandle(), application = Application())
        )
    }
}

@Preview(showBackground = true)
@Composable
fun RoomsPreview() {
    VaChercherTheme {
        RoomsView(
            themeId = "HOUSE",
            mainViewModel = viewModel(),
            enterRoom = {},
            createRoom = {},
        )
    }
}