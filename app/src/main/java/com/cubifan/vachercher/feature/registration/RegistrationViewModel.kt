package com.cubifan.vachercher.feature.registration

import androidx.compose.material.SnackbarHostState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.ui.text.input.TextFieldValue
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.cubifan.vachercher.core.api.requests.UserCreationRequest
import com.cubifan.vachercher.core.di.DependencyInjector
import com.cubifan.vachercher.utils.UserInfos
import com.cubifan.vachercher.utils.showNewSnackbar
import com.google.firebase.FirebaseNetworkException
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException
import com.google.firebase.auth.FirebaseAuthUserCollisionException
import com.google.firebase.auth.FirebaseAuthWeakPasswordException
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.launch

class RegistrationViewModel : ViewModel() {
    val login = mutableStateOf(TextFieldValue())
    val password = mutableStateOf(TextFieldValue())
    val confirmPassword = mutableStateOf(TextFieldValue())
    val pseudo = mutableStateOf(TextFieldValue())
    val userCreated = mutableStateOf(false)
    val snackbarHostState = mutableStateOf(SnackbarHostState())

    fun createUser(hapticAction: () -> Unit) {
        try {
            viewModelScope.launch(IO) {
                if (pseudo.value.text.isBlank()) {
                    snackbarHostState.value.showNewSnackbar(
                        "Le pseudo ne doit pas être vide",
                        hapticAction
                    )
                } else if (!pseudo.value.text.matches(Regex("^[A-z0-9]+$"))) {
                    snackbarHostState.value.showNewSnackbar(
                        "Le pseudo doit comporter des caractères alphanumériques",
                        hapticAction
                    )
                } else if (login.value.text.isBlank()) {
                    snackbarHostState.value.showNewSnackbar(
                        "Veuillez entrer votre mail",
                        hapticAction
                    )
                } else if (password.value.text.isBlank() || confirmPassword.value.text.isBlank()) {
                    snackbarHostState.value.showNewSnackbar(
                        "Veuillez ajouter et confirmer votre mot de passe",
                        hapticAction
                    )
                } else if (confirmPassword.value != password.value) {
                    snackbarHostState.value.showNewSnackbar(
                        "Les champs de mot de passe sont différents",
                        hapticAction
                    )
                } else {
                    Firebase.auth.createUserWithEmailAndPassword(
                        login.value.text,
                        password.value.text
                    ).addOnCompleteListener {
                        val tag = "User creation"
                        if (it.isSuccessful) {
                            registerToApi(hapticAction)
                        } else {
                            viewModelScope.launch {
                                when (it.exception) {
                                    is FirebaseNetworkException ->
                                        snackbarHostState.value.showNewSnackbar(
                                            "Vérifiez votre connexion",
                                            hapticAction
                                        )
                                    is FirebaseAuthUserCollisionException ->
                                        registerToApi(hapticAction)
                                    is FirebaseAuthWeakPasswordException ->
                                        snackbarHostState.value.showNewSnackbar(
                                            "Le mot de passe doit comprendre au moins 6 caractères",
                                            hapticAction
                                        )
                                    is FirebaseAuthInvalidCredentialsException ->
                                        snackbarHostState.value.showNewSnackbar(
                                            "Veuillez entrer un mail valide",
                                            hapticAction
                                        )
                                    else -> snackbarHostState.value.showNewSnackbar(
                                        "Inscription échouée. Vérifiez les informations transmises",
                                        hapticAction
                                    )
                                }
                            }
                        }
                    }
                }
            }
        } catch (e: Exception) {
            viewModelScope.launch {
                snackbarHostState.value.showSnackbar("Veuillez remplir tous les champs correctement")
            }
        }
    }

    private fun registerToApi(haptiqueAction: () -> Unit) {
        viewModelScope.launch {
            val registerResponse =
                DependencyInjector.API_SERVICE.register(
                    UserCreationRequest(
                        uid = UserInfos.uid,
                        nickname = pseudo.value.text.trim(),
                        email = login.value.text
                    )
                )

            registerResponse.body()?.let { response ->
                if (response.successful) {
                    userCreated.value = true
                } else {
                    snackbarHostState.value.showNewSnackbar(response.message!!, haptiqueAction)
                }
            } ?: run {
                snackbarHostState.value.showNewSnackbar(
                    "Une erreur inattendue s'est produite. Veuillez réessayer.",
                    haptiqueAction
                )
            }
        }
    }
}