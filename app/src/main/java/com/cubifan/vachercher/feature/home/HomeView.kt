package com.cubifan.vachercher.feature.home

import android.annotation.SuppressLint
import android.media.MediaPlayer
import androidx.compose.animation.core.Spring
import androidx.compose.animation.core.animateIntOffsetAsState
import androidx.compose.animation.core.spring
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowForward
import androidx.compose.material.icons.filled.Person
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.IntOffset
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.cubifan.vachercher.R
import com.cubifan.vachercher.core.components.Leaderboard
import com.cubifan.vachercher.core.data.PastGame
import com.cubifan.vachercher.ui.theme.DeepOrange
import com.cubifan.vachercher.utils.UserInfos

@Composable
fun HomeView(
    pastGames: List<PastGame>,
    mainSnackbarHostState: SnackbarHostState,
    goToThemeSelection: () -> Unit,
    showLeaderboard: () -> Unit,
    showProfile: () -> Unit,
    showBadges: () -> Unit,
) {
    val context = LocalContext.current
    val clicSound = MediaPlayer.create(context, R.raw.clic)
    var isAnim by remember { mutableStateOf(false) }
    val playOffset = animateIntOffsetAsState(
        targetValue = if (isAnim) IntOffset(x = -50, y = 0)
        else IntOffset(x = -800, y = 0),
        animationSpec = spring(dampingRatio = .64f, stiffness = Spring.StiffnessLow)
    )
    val profileOffset = animateIntOffsetAsState(
        targetValue = if (isAnim) IntOffset(x = -50, y = 0)
        else IntOffset(x = -700, y = 0),
        animationSpec = spring(dampingRatio = .7f, stiffness = Spring.StiffnessLow)
    )
    val badgeOffset = animateIntOffsetAsState(
        targetValue = if (isAnim) IntOffset(x = 50, y = 0)
        else IntOffset(x = 700, y = 0),
        animationSpec = spring(dampingRatio = .7f, stiffness = Spring.StiffnessLow)
    )
    val dashboardOffset = animateIntOffsetAsState(
        targetValue = if (isAnim) IntOffset(x = -50, y = 0)
        else IntOffset(x = -600, y = 0),
        animationSpec = spring(dampingRatio = .72f, stiffness = Spring.StiffnessLow)
    )

    LaunchedEffect(Unit) {
        isAnim = true
    }

    Column(
        verticalArrangement = Arrangement.spacedBy(32.dp),
        horizontalAlignment = Alignment.CenterHorizontally,
        modifier = Modifier
            .fillMaxSize()
            .padding(top = 32.dp),
    ) {
        Button(
            onClick = { goToThemeSelection(); clicSound.start() },
            contentPadding = PaddingValues(top = 12.dp, bottom = 12.dp, start = 40.dp, end = 20.dp),
            shape = RoundedCornerShape(topEnd = 32.dp, bottomEnd = 32.dp),
            modifier = Modifier
                .align(Alignment.Start)
                .fillMaxWidth(.9f)
                .height(120.dp)
                .offset { playOffset.value }
        ) {
            Row(
                verticalAlignment = Alignment.CenterVertically,
                horizontalArrangement = Arrangement.SpaceBetween,
                modifier = Modifier.fillMaxWidth(),
            ) {
                Text(
                    text = "Jouer",
                    fontSize = 36.sp,
                    textAlign = TextAlign.Center,
                    color = Color.White,
                )
                Icon(
                    imageVector = Icons.Default.ArrowForward,
                    contentDescription = null,
                    tint = Color.White,
                    modifier = Modifier.size(80.dp)
                )
            }
        }

        Row {
            Button(
                onClick = showProfile,
                contentPadding = PaddingValues(
                    top = 12.dp,
                    bottom = 12.dp,
                    start = 40.dp,
                    end = 20.dp
                ),
                shape = RoundedCornerShape(topEnd = 32.dp, bottomEnd = 32.dp),
                modifier = Modifier
                    .fillMaxWidth(.7f)
                    .height(95.dp)
                    .offset { profileOffset.value }
            ) {
                Row(
                    verticalAlignment = Alignment.CenterVertically,
                    horizontalArrangement = Arrangement.SpaceBetween,
                    modifier = Modifier.fillMaxWidth(),
                ) {
                    Column {
                        Text(
                            text = UserInfos.pseudo,
                            fontSize = 24.sp,
                            textAlign = TextAlign.Center
                        )
                        Text(
                            text = "Niv. ${UserInfos.level}",
                            fontSize = 22.sp,
                            textAlign = TextAlign.Center,
                            color = Color.White,
                            fontWeight = FontWeight.SemiBold,
                        )

                    }
                    Icon(
                        imageVector = Icons.Default.Person,
                        contentDescription = null,
                        tint = Color.White,
                        modifier = Modifier.size(40.dp)
                    )
                }
            }
            Spacer(Modifier.weight(1f))
            Button(
                onClick = showBadges,
                contentPadding = PaddingValues(top = 12.dp, bottom = 12.dp, end = 20.dp),
                shape = RoundedCornerShape(topStart = 32.dp, bottomStart = 32.dp),
                modifier = Modifier
                    .fillMaxWidth(.8f)
                    .height(95.dp)
                    .offset { badgeOffset.value },
            ) {
                Icon(
                    painterResource(id = R.drawable.medal),
                    contentDescription = null,
                    tint = Color.White,
                    modifier = Modifier.size(40.dp)
                )
            }
        }
        Button(
            onClick = { showLeaderboard(); clicSound.start() },
            contentPadding = PaddingValues(top = 12.dp, bottom = 12.dp, start = 40.dp, end = 20.dp),
            shape = RoundedCornerShape(topEnd = 32.dp, bottomEnd = 32.dp),
            modifier = Modifier
                .align(Alignment.Start)
                .fillMaxWidth(.65f)
                .height(70.dp)
                .offset { dashboardOffset.value },
        ) {
            Row(
                verticalAlignment = Alignment.CenterVertically,
                horizontalArrangement = Arrangement.SpaceBetween,
                modifier = Modifier.fillMaxWidth(),
            ) {
                Text(
                    text = "Classement",
                    fontSize = 20.sp,
                    textAlign = TextAlign.Center,
                    color = Color.White,
                )
                Icon(
                    painterResource(id = R.drawable.baseline_leaderboard_24),
                    contentDescription = null,
                    tint = Color.White,
                    modifier = Modifier.size(40.dp),
                )
            }
        }
        Leaderboard(
            header = listOf("Date", "Thème", "Score", "Rang"),
            rows = pastGames
        )
    }
    SnackbarHost(
        hostState = mainSnackbarHostState,
        snackbar = { Snackbar(it, backgroundColor = DeepOrange) }
    )
}

@SuppressLint("UnrememberedMutableState")
@Preview(showBackground = true, showSystemUi = true)
@Composable
fun HomePreview() {
    HomeView(
        listOf(),
        //mutableStateListOf(PastGame(100, "01/01/2023", "1/3", "Salon")),
        SnackbarHostState(),
        {},
        {},
        {},
        {}
    )
}