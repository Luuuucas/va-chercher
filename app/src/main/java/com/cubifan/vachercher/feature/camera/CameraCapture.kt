package com.cubifan.vachercher.feature.camera

import android.content.Context
import android.media.MediaPlayer
import android.util.Log
import android.util.Size
import androidx.camera.core.*
import androidx.camera.lifecycle.ProcessCameraProvider
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalLifecycleOwner
import androidx.compose.ui.unit.dp
import androidx.core.content.ContextCompat
import com.cubifan.vachercher.MainViewModel
import com.cubifan.vachercher.R
import com.cubifan.vachercher.core.components.CustomTimer
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.io.File
import java.util.concurrent.Executor
import kotlin.coroutines.resume
import kotlin.coroutines.resumeWithException
import kotlin.coroutines.suspendCoroutine

@Composable
fun CameraCapture(
    cameraSelector: CameraSelector = CameraSelector.DEFAULT_BACK_CAMERA,
    onImageFile: (File) -> Unit = { },
    itemToSearch: String,
    viewModel: CameraViewModel,
    mainViewModel: MainViewModel
) {
    val context = LocalContext.current

    LaunchedEffect(itemToSearch) {
        MediaPlayer.create(context, R.raw.item_to_find).start()
    }

    Box(modifier = Modifier.fillMaxSize()) {
        val lifecycleOwner = LocalLifecycleOwner.current
        val coroutineScope = rememberCoroutineScope()
        var previewUseCase by remember { mutableStateOf<UseCase>(Preview.Builder().build()) }
        val imageCaptureUseCase by remember {
            mutableStateOf(
                ImageCapture.Builder()
                    .setCaptureMode(ImageCapture.CAPTURE_MODE_MINIMIZE_LATENCY)
                    .build()
            )
        }
        Box {
            CameraPreview(
                modifier = Modifier.fillMaxSize(),
                onUseCase = {
                    previewUseCase = it
                }
            )
            CustomSnackbar(objectToFind = itemToSearch, Modifier.align(Alignment.TopCenter))
            CustomTimer(
                time = viewModel.timeLeft.value,
                round = mainViewModel.roomPublicDataState.value?.actualRound ?: 1,
                modifier = Modifier
                    .align(Alignment.TopStart)
                    .offset(y = 80.dp)
                    .padding(top = 8.dp),
                shape = RoundedCornerShape(topEnd = 14.dp, bottomEnd = 14.dp)
            )
            CapturePictureButton(
                modifier = Modifier
                    .size(130.dp)
                    .padding(20.dp)
                    .align(Alignment.BottomCenter),
                onClick = {
                    mainViewModel.foundedState.value = MainViewModel.FoundedState.PROCESSING
                    viewModel.pictureTakingIsEnabled.value = false
                    coroutineScope.launch {
                        onImageFile(imageCaptureUseCase.takePicture(context.mainExecutor))
                    }
                    MediaPlayer.create(context, R.raw.photo).start()
                },
                enabled = viewModel.timeLeft.value > 1 && viewModel.pictureTakingIsEnabled.value
            )
        }
        LaunchedEffect(previewUseCase) {
            val cameraProvider = context.getCameraProvider()
            try {
                cameraProvider.unbindAll()
                cameraProvider.bindToLifecycle(
                    lifecycleOwner,
                    cameraSelector,
                    previewUseCase,
                    imageCaptureUseCase,
                    ImageAnalysis.Builder()
                        .setTargetResolution(Size(1280, 720))
                        .build()
                )
                mainViewModel.gamePhaseTime.collect {
                    viewModel.timeLeft.value = it / 1000
                }
            } catch (ex: Exception) {
                Log.e("CameraCapture", "Failed to bind camera use cases", ex)
            }
        }
    }
}

private suspend fun Context.getCameraProvider(): ProcessCameraProvider =
    suspendCoroutine { continuation ->
        ProcessCameraProvider.getInstance(this).also { cameraProvider ->
            cameraProvider.addListener({
                continuation.resume(cameraProvider.get())
            }, ContextCompat.getMainExecutor(this))
        }
    }

private suspend fun ImageCapture.takePicture(executor: Executor): File {
    val photoFile = withContext(Dispatchers.IO) {
        kotlin.runCatching {
            File.createTempFile("image", "jpg")
        }.getOrElse {
            File("/dev/null")
        }
    }

    return suspendCoroutine { continuation ->
        val outputOptions = ImageCapture.OutputFileOptions.Builder(photoFile).build()
        takePicture(
            outputOptions, executor,
            object : ImageCapture.OnImageSavedCallback {
                override fun onImageSaved(output: ImageCapture.OutputFileResults) {
                    continuation.resume(photoFile)
                }

                override fun onError(ex: ImageCaptureException) {
                    continuation.resumeWithException(ex)
                }
            }
        )
    }
}