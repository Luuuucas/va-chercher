package com.cubifan.vachercher.feature.camera

import android.app.Application
import android.graphics.Bitmap
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.viewModelScope
import com.cubifan.vachercher.MainViewModel
import com.cubifan.vachercher.utils.UserInfos
import id.zelory.compressor.Compressor
import id.zelory.compressor.constraint.format
import id.zelory.compressor.constraint.quality
import id.zelory.compressor.constraint.resolution
import kotlinx.coroutines.*
import java.io.File

class CameraViewModel(application: Application) : AndroidViewModel(application) {
    var pictureSended by mutableStateOf(false)
    val timeLeft = mutableStateOf(30L)
    var imageTaken: File? = null
    private var compressedImage: Deferred<File>? = null
    var pictureTakingIsEnabled = mutableStateOf(true)


    fun delayPictureTaking() {
        viewModelScope.launch {
            delay(200L)
            pictureTakingIsEnabled.value = true
        }
    }

    fun prepareImage(imageTaken: File) {
        compressedImage?.cancel()
        compressedImage = viewModelScope.async(Dispatchers.IO) {
            Compressor.compress(getApplication(), imageTaken) {
                resolution(1280, 720)
                quality(10)
                format(Bitmap.CompressFormat.JPEG)
            }
        }
    }

    fun sendImage(mainViewModel: MainViewModel) {
        viewModelScope.launch(Dispatchers.IO) {
            compressedImage?.let {
                mainViewModel.prepareAndSendImageToApi(
                    "${UserInfos.pseudo}-${System.currentTimeMillis()}.jpg",
                    it
                )
            }
        }
        pictureSended = true
    }
}