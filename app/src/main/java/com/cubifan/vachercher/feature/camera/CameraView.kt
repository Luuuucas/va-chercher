package com.cubifan.vachercher.feature.camera

import android.media.MediaPlayer
import androidx.activity.compose.BackHandler
import androidx.compose.animation.animateColor
import androidx.compose.animation.core.FastOutLinearInEasing
import androidx.compose.animation.core.infiniteRepeatable
import androidx.compose.animation.core.rememberInfiniteTransition
import androidx.compose.animation.core.tween
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowForward
import androidx.compose.material.icons.filled.Check
import androidx.compose.material.icons.filled.Refresh
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.Color.Companion.White
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.core.net.toUri
import androidx.lifecycle.viewmodel.compose.viewModel
import coil.compose.rememberAsyncImagePainter
import com.cubifan.vachercher.EMPTY_IMAGE_URI
import com.cubifan.vachercher.MainViewModel
import com.cubifan.vachercher.R
import com.cubifan.vachercher.core.components.CustomPopup
import com.cubifan.vachercher.core.components.CustomTimer
import com.cubifan.vachercher.core.ws.models.Room
import com.cubifan.vachercher.ui.theme.DeepOrange
import com.cubifan.vachercher.ui.theme.Orange
import kotlinx.coroutines.launch

@Composable
fun CameraView(
    showResults: () -> Unit,
    quitGame: () -> Unit,
    viewModel: CameraViewModel = viewModel(),
    mainViewModel: MainViewModel
) {
    BackHandler(true) { mainViewModel.popupShowing.value = true }
    CustomPopup(
        showing = mainViewModel.popupShowing,
        allowDismissRequest = true,
        titleText = "Quitter ?",
        textText = "Es-tu sûr de quitter la partie?",
        confirmButtonText = "Oui",
        dismissButtonText = "Non",
        confirmButtonAction = quitGame,
        dismissButtonAction = {}
    )

    LaunchedEffect(Unit) {
        mainViewModel.snackbarHostState.value.currentSnackbarData?.dismiss()
        mainViewModel.collectSocketEvents()
        mainViewModel.collectConnectionEvents()
        launch {
            mainViewModel.currentGamePhase.collect { value ->
                when (value.phase) {
                    Room.GamePhase.SHOW_RESULTS -> {
                        showResults()
                    }
                    else -> Unit
                }
            }
        }
        launch {
            mainViewModel.gamePhaseTime.collect {
                viewModel.timeLeft.value = it / 1000
            }
        }
        launch {
            mainViewModel.gotKicked.collect {
                if (it) quitGame()
            }
        }
    }

    var imageUri by remember { mutableStateOf(EMPTY_IMAGE_URI) }
    if (imageUri != EMPTY_IMAGE_URI) {
        Box(modifier = Modifier.fillMaxSize()) {
            Image(
                modifier = Modifier.fillMaxSize(),
                painter = rememberAsyncImagePainter(imageUri),
                contentDescription = "Captured image"
            )
            CustomTimer(
                time = viewModel.timeLeft.value,
                round = mainViewModel.roomPublicDataState.value?.actualRound ?: 1,
                modifier = Modifier
                    .align(Alignment.TopCenter)
                    .padding(top = 25.dp)
            )
            Row(
                modifier = Modifier
                    .fillMaxWidth()
                    .align(Alignment.BottomCenter)
                    .padding(bottom = 28.dp),
                horizontalArrangement = Arrangement.SpaceEvenly
            ) {
                if (!viewModel.pictureSended) {
                    val clicSound = MediaPlayer.create(LocalContext.current, R.raw.clic)
                    Button(
                        modifier = Modifier.width(IntrinsicSize.Max),
                        contentPadding = PaddingValues(top = 12.dp, bottom = 12.dp, start = 40.dp, end = 20.dp),
                        onClick = {
                            imageUri = EMPTY_IMAGE_URI
                            viewModel.delayPictureTaking()
                            clicSound.start()
                        }) {
                        Row(
                            verticalAlignment = Alignment.CenterVertically,
                            horizontalArrangement = Arrangement.spacedBy(8.dp)
                        ) {
                            Icon(
                                Icons.Default.Refresh,
                                "Reprendre une photo",
                                modifier = Modifier.size(32.dp)
                            )
                            Text("Reprendre", fontWeight = FontWeight.Bold, maxLines = 1)
                        }
                    }
                    val animation = rememberInfiniteTransition()
                    val color = animation.animateColor(
                        initialValue = White,
                        targetValue = Orange,
                        animationSpec = infiniteRepeatable(
                            animation = tween(
                                durationMillis = 1000,
                                easing = FastOutLinearInEasing
                            )
                        )
                    )
                    Button(
                        modifier = Modifier.width(IntrinsicSize.Max),
                        contentPadding = PaddingValues(top = 12.dp, bottom = 12.dp, start = 40.dp, end = 20.dp),
                        onClick = {
                            viewModel.sendImage(mainViewModel)
                            clicSound.start()
                        },
                        colors = ButtonDefaults.buttonColors(backgroundColor = color.value)
                    ) {
                        Row(
                            verticalAlignment = Alignment.CenterVertically,
                            horizontalArrangement = Arrangement.spacedBy(8.dp)
                        ) {
                            Icon(
                                Icons.Default.ArrowForward,
                                contentDescription = "Envoyer",
                                modifier = Modifier.size(32.dp)
                            )
                            Text("Envoyer", fontWeight = FontWeight.Bold, maxLines = 1)
                        }
                    }
                } else {
                    val context = LocalContext.current
                    val correctSound = MediaPlayer.create(context, R.raw.correct)
                    val wrongSound = MediaPlayer.create(context, R.raw.wrong)

                    LaunchedEffect(mainViewModel.foundedState.value) {
                        when (mainViewModel.foundedState.value) {
                            MainViewModel.FoundedState.FOUNDED -> correctSound.start()
                            MainViewModel.FoundedState.NOT_FOUNDED -> wrongSound.start()
                        }
                    }
                    when (mainViewModel.foundedState.value) {
                        MainViewModel.FoundedState.FOUNDED ->
                            Icon(
                                modifier = Modifier.size(40.dp),
                                imageVector = Icons.Filled.Check,
                                contentDescription = "check",
                                tint = Color.Green,
                            )
                        MainViewModel.FoundedState.NOT_FOUNDED ->
                            Icon(
                                modifier = Modifier.size(40.dp),
                                painter = painterResource(R.drawable.cross),
                                contentDescription = "cross",
                                tint = Color.Red
                            )
                        else -> Text(
                            "En attente ...",
                            fontSize = 24.sp,
                            fontWeight = FontWeight.SemiBold
                        )
                    }
                }
            }
        }
    } else {
        Box(modifier = Modifier.fillMaxSize()) {
            CameraCapture(
                onImageFile = { file ->
                    viewModel.imageTaken = file
                    imageUri = file.toUri()

                    viewModel.prepareImage(file)
                },
                itemToSearch = mainViewModel.itemToSearch.value,
                viewModel = viewModel,
                mainViewModel = mainViewModel
            )
        }
    }

    SnackbarHost(
        hostState = mainViewModel.snackbarHostState.value,
        snackbar = { Snackbar(it, backgroundColor = DeepOrange) }
    )
}
