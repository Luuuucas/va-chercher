package com.cubifan.vachercher.feature.onboarding

import androidx.annotation.DrawableRes
import com.cubifan.vachercher.R

sealed class OnboardingPage(
    @DrawableRes
    val image: Int,
    val title: String,
    val description: String
) {
    object First : OnboardingPage(
        image = R.drawable.ballon,
        title = "Prépare toi",
        description = "L'application te donne un objet à aller chercher"
    )

    object Second : OnboardingPage(
        image = R.drawable.party,
        title = "Cherche",
        description = "Prends en photo cet objet le plus vite possible"
    )

    object Third : OnboardingPage(
        image = R.drawable.happy,
        title = "Sois victorieux",
        description = "Gagne des points et grimpe dans le classement"
    )

    object Fourth : OnboardingPage(
        image = R.drawable.chat,
        title = "Va Chercher !",
        description = "Joue en groupe et partage tes scores"
    )
}
