package com.cubifan.vachercher.feature.dashboard

import android.media.MediaPlayer
import android.widget.Toast
import androidx.activity.compose.BackHandler
import androidx.compose.foundation.Canvas
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Share
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.geometry.Size
import androidx.compose.ui.graphics.drawscope.translate
import androidx.compose.ui.platform.LocalConfiguration
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.viewmodel.compose.viewModel
import com.airbnb.lottie.compose.*
import com.cubifan.vachercher.MainViewModel
import com.cubifan.vachercher.R
import com.cubifan.vachercher.core.components.CustomPopup
import com.cubifan.vachercher.core.components.DownRoundButton
import com.cubifan.vachercher.core.components.LittleSpacer
import com.cubifan.vachercher.core.components.MidSpacer
import com.cubifan.vachercher.core.ws.models.PlayerPublicData
import com.cubifan.vachercher.core.ws.models.Room
import com.cubifan.vachercher.ui.theme.*
import com.cubifan.vachercher.utils.UserInfos
import kotlinx.coroutines.launch

@Composable
fun DashboardView(
    goToNewRound: () -> Unit,
    quitGame: () -> Unit = {},
    mainViewModel: MainViewModel = viewModel()
) {
    val timeLeft = remember { mutableStateOf(30L) }
    BackHandler(true) { mainViewModel.popupShowing.value = true }

    CustomPopup(
        showing = mainViewModel.popupShowing,
        allowDismissRequest = true,
        titleText = "Quitter ?",
        textText = "Es-tu sûr de quitter la partie?",
        confirmButtonText = "Oui",
        dismissButtonText = "Non",
        confirmButtonAction = quitGame,
        dismissButtonAction = {}
    )

    LaunchedEffect("GamePhaseCollection") {
        mainViewModel.collectSocketEvents()
        mainViewModel.collectConnectionEvents()

        launch {
            mainViewModel.gamePhaseTime.collect {
                timeLeft.value = it / 1000
            }
        }
        launch {
            mainViewModel.gotKicked.collect {
                if (it) quitGame()
            }
        }
        launch {
            mainViewModel.currentGamePhase.collect { value ->
                when (value.phase) {
                    Room.GamePhase.SEARCH -> {
                        goToNewRound()
                    }
                    else -> Unit
                }
            }
        }
    }

    mainViewModel.roomPublicDataState.value?.let { roomData ->
        val context = LocalContext.current
        LazyColumn(
            horizontalAlignment = Alignment.CenterHorizontally,
            modifier = Modifier.fillMaxSize()
        ) {
            if (roomData.endOfGame) {
                item {
                    Podium(roomData.playersPublicData)
                    DownRoundButton(
                        onClick = quitGame,
                        text = context.getString(R.string.menu),
                        Modifier.padding(20.dp)
                    )
                }
                item {
                    val toast = Toast.makeText(context, "Score copié !", Toast.LENGTH_SHORT)
                    val userScore = roomData.playersPublicData.find { it.username == UserInfos.pseudo }?.score ?: 0
                    val textToCopy = stringResource(R.string.score_copy, userScore, UserInfos.score + userScore)
                    IconButton(onClick = { mainViewModel.copyGame(context, toast, textToCopy) }) {
                        Icon(imageVector = Icons.Filled.Share, "Partager")
                    }
                }
            } else {
                item {
                    Text(
                        text = "Round ${roomData.actualRound} / ${roomData.numberOfRounds}",
                        fontSize = 24.sp,
                        fontWeight = FontWeight.SemiBold
                    )
                    MidSpacer()
                    Text("Prochain round dans ${timeLeft.value}...")
                }
            }

            items(roomData.playersPublicData) { playerPublicData ->
                LittleSpacer()
                PlayerPoints(playerPublicData)
            }
        }
    }

}

@Composable
fun PlayerPoints(playerPublicData: PlayerPublicData) {
    Surface(shape = RoundedCornerShape(8.dp), color = MaterialTheme.colors.primary) {
        Box(
            Modifier
                .fillMaxWidth(.9f)
                .height(60.dp)
        ) {
            Row(
                verticalAlignment = Alignment.CenterVertically,
                modifier = Modifier.fillMaxSize()
            ) {
                Text(
                    text = playerPublicData.username, modifier = Modifier
                        .weight(1f)
                        .padding(start = 16.dp)
                )
                Text(text = "${playerPublicData.score} pts", modifier = Modifier.weight(.5f))
                if (playerPublicData.score != 0) {
                    Icon(
                        painter = painterResource(R.drawable.trophy),
                        tint = when (playerPublicData.rank) {
                            1 -> Gold
                            2 -> Silver
                            3 -> Bronze
                            else -> Chocolate
                        },
                        contentDescription = "trophy",
                        modifier = Modifier.weight(.5f)
                            .padding(8.dp)
                            .clip(RoundedCornerShape(8.dp))
                            .background(MaterialTheme.colors.background)
                    )
                } else {
                    Text(text = "😅", modifier = Modifier.weight(.5f), textAlign = TextAlign.Center)
                }
            }
        }
    }
}

@Composable
private fun Podium(players: List<PlayerPublicData>) {
    val width = LocalConfiguration.current.screenWidthDp.toFloat() / 1.3f
    val height = 500f
    Box(
        contentAlignment = Alignment.TopCenter,
        modifier = Modifier
            .fillMaxSize()
            .background(Orange)
    ) {
        Canvas(
            Modifier
                .height(400.dp)
                .padding(20.dp)
        ) {
            translate(top = 200f) {
                drawRect(
                    color = Gold,
                    topLeft = Offset(-width / 2, 0f),
                    size = Size(width, height)
                )
                if (players.size > 2) {
                    drawRect(
                        color = Silver,
                        topLeft = Offset(-width * 1.5f, height * 1.3f / 2.3f),
                        size = Size(width, height / 2.3f),
                    )
                    drawRect(
                        color = Bronze,
                        topLeft = Offset(width / 2, height * 2 / 3),
                        size = Size(width, height / 3)
                    )
                }
            }
        }
        Confettis()
        Text(
            text = players[0].username,
            fontSize = 30.sp,
            fontWeight = FontWeight.Bold,
            modifier = Modifier
                .offset(x = 0.dp, y = 40.dp)
        )
        Icon(
            painter = painterResource(id = R.drawable.trophy),
            null,
            modifier = Modifier
                .offset(x = 0.dp, y = 110.dp)
                .size(40.dp)
        )
        if (players.size > 2) {
            Text(
                text = players[1].username,
                fontSize = 20.sp,
                modifier = Modifier
                    .offset(x = (-width / 3).dp, y = 150.dp)
            )
            Icon(
                painter = painterResource(id = R.drawable.trophy),
                null,
                modifier = Modifier
                    .offset(x = (-width / 3).dp, y = 205.dp)
                    .size(30.dp)
            )
            Text(
                text = players[2].username,
                fontSize = 20.sp,
                modifier = Modifier
                    .offset(x = (width / 3).dp, y = 170.dp)
            )
            Icon(
                painter = painterResource(id = R.drawable.trophy),
                null,
                modifier = Modifier
                    .offset(x = (width / 3).dp, y = 210.dp)
                    .size(30.dp)
            )
        }
    }
}

@Composable
private fun Confettis() {
    val composition by rememberLottieComposition(spec = LottieCompositionSpec.RawRes(R.raw.confettis))
    val progress by animateLottieCompositionAsState(
        composition = composition,
        iterations = LottieConstants.IterateForever
    )
    val applauseSound = MediaPlayer.create(LocalContext.current, R.raw.applause)

    LaunchedEffect(Unit) {
        applauseSound.start()
    }

    Box(
        contentAlignment = Alignment.TopCenter, modifier = Modifier
            .fillMaxWidth()
            .height(400.dp)
    ) {
        LottieAnimation(
            composition = composition,
            progress = { progress }
        )
    }
}

@Preview(heightDp = 300)
@Composable
fun PodiumPreview() {
    val players = listOf(
        PlayerPublicData("Lucas", 1000),
        PlayerPublicData("Yannis", 300),
        PlayerPublicData("Norman", 20)
    )
    val onePlayer = listOf(PlayerPublicData("Lucas", 1000))
    Podium(players)
}

@Preview(widthDp = 300)
@Composable
fun PlayerPointsPreview() {
    PlayerPoints(PlayerPublicData("Lucas", 100, 1))
}

@Preview(showSystemUi = true)
@Composable
fun DashboardPreview() {
    DashboardView({})
}