package com.cubifan.vachercher.feature.login

import android.content.res.Configuration
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Snackbar
import androidx.compose.material.SnackbarHost
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.hapticfeedback.HapticFeedbackType
import androidx.compose.ui.platform.LocalConfiguration
import androidx.compose.ui.platform.LocalHapticFeedback
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.viewmodel.compose.viewModel
import com.cubifan.vachercher.MainViewModel
import com.cubifan.vachercher.R
import com.cubifan.vachercher.core.components.Credentials
import com.cubifan.vachercher.core.components.DownRoundButton
import com.cubifan.vachercher.core.components.LittleSpacer
import com.cubifan.vachercher.core.components.RunningPeople
import com.cubifan.vachercher.ui.theme.DeepOrange
import com.cubifan.vachercher.ui.theme.Orange
import com.cubifan.vachercher.ui.theme.Success
import com.cubifan.vachercher.ui.theme.VaChercherTheme

@Composable
fun LoginView(
    login: () -> Unit,
    register: () -> Unit,
    viewModel: LoginViewModel = viewModel(),
    mainViewModel: MainViewModel
) {
    val isPortrait = LocalConfiguration.current.orientation == Configuration.ORIENTATION_PORTRAIT
    val haptic = LocalHapticFeedback.current

    if (viewModel.logged.value) login(); viewModel.logged.value = false

    Box(
        modifier = Modifier
            .fillMaxSize()
            .background(Orange)
    ) {
        Column(
            horizontalAlignment = Alignment.CenterHorizontally,
            modifier = Modifier
                .verticalScroll(rememberScrollState())
                .padding(30.dp)
                .fillMaxSize()
        ) {
            if (isPortrait) {
                Surface(shape = RoundedCornerShape(16.dp)) {
                    Image(
                        painter = painterResource(id = R.drawable.ic_launcher),
                        contentDescription = null,
                        modifier = Modifier
                            .size(300.dp)
                            .background(Orange)
                    )
                }
            }
            Text(
                text = stringResource(id = R.string.sign_in),
                fontWeight = FontWeight.SemiBold,
                style = TextStyle(color = MaterialTheme.colors.primary, fontSize = 30.sp)
            )
            Credentials(viewModel.login, viewModel.password, viewModel.snackbarHostState.value)
            LittleSpacer()
            DownRoundButton(
                { viewModel.signIn { haptic.performHapticFeedback(HapticFeedbackType.LongPress) } },
                stringResource(R.string.play),
                color = DeepOrange
            )
            LittleSpacer()
            Text(
                text = stringResource(id = R.string.ask_no_account),
                color = Color.White,
                fontSize = 24.sp
            )
            DownRoundButton(
                onClick = register,
                text = stringResource(id = R.string.sign_up),
                color = DeepOrange
            )
        }
        RunningPeople(Color.White)
        SnackbarHost(
            hostState = mainViewModel.snackbarHostState.value,
            snackbar = { Snackbar(snackbarData = it, backgroundColor = Success) }
        )
    }
}


@Preview(showBackground = true)
@Composable
fun LoginPreview() {
    VaChercherTheme {
        LoginView({}, {}, mainViewModel = viewModel())
    }
}