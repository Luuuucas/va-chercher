package com.cubifan.vachercher.feature.theme_game

import android.media.MediaPlayer
import android.widget.Toast
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.graphics.ColorMatrix
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.viewmodel.compose.viewModel
import coil.compose.AsyncImage
import coil.request.ImageRequest
import com.cubifan.vachercher.MainViewModel
import com.cubifan.vachercher.R
import com.cubifan.vachercher.core.components.RunningPeople
import com.cubifan.vachercher.feature.theme_game.data.Theme
import com.cubifan.vachercher.ui.theme.DeepOrange
import com.cubifan.vachercher.utils.UserInfos
import java.net.URL

@Composable
fun ThemeGameView(
    goToRoomCreation: (Int) -> Unit,
    viewModel: ThemeViewModel = viewModel(),
    mainViewModel: MainViewModel
) {

    LaunchedEffect(Unit) {
        viewModel.firebaseTokenIdExpired.collect {
            if (it) {
                mainViewModel.firebaseTokenIdExpired.value = it
                viewModel.firebaseTokenIdExpired.value = false
            }
        }
    }

    Box(Modifier.fillMaxSize()) {
        RunningPeople()

        if (!viewModel.apiCallInProgress && viewModel.themes.size == 0) {
            Button(
                onClick = { viewModel.getThemes() },
                contentPadding = PaddingValues(vertical = 16.dp),
                shape = RoundedCornerShape(16.dp),
                modifier = Modifier
                    .align(Alignment.Center)
                    .fillMaxWidth()
            ) {
                Text(
                    text = stringResource(R.string.load_themes),
                    fontSize = 16.sp,
                    textAlign = TextAlign.Center
                )
            }
        }

        if (viewModel.apiCallInProgress) {
            LinearProgressIndicator(Modifier.fillMaxWidth())
        } else {
            Column(
                modifier = Modifier
                    .verticalScroll(rememberScrollState())
                    .padding(top = 64.dp),
                verticalArrangement = Arrangement.spacedBy(16.dp),
            ) {
                viewModel.themes.forEach {
                    ThemeCard(
                        modifier = Modifier.padding(horizontal = 24.dp),
                        goToRoomCreation = goToRoomCreation,
                        card = it
                    )
                }
            }
        }
    }

}

@Composable
private fun ThemeCard(
    modifier: Modifier = Modifier,
    goToRoomCreation: (Int) -> Unit,
    card: Theme,
) {
    val context = LocalContext.current
    val isLocked = UserInfos.level < card.levelRequired
    val toast = Toast.makeText(context, "Thème non débloqué !", Toast.LENGTH_SHORT)
    val clicSound = MediaPlayer.create(context, R.raw.clic)

    Card(
        shape = RoundedCornerShape(8.dp),
        modifier = modifier.clickable {
            clicSound.start()
            if (isLocked) toast.show()
            else goToRoomCreation(card.id)
        },
    ) {
        Box(Modifier.fillMaxSize()) {
            AsyncImage(
                model = ImageRequest.Builder(context)
                    .data(card.image.toString())
                    .crossfade(true)
                    .build(),
                contentScale = ContentScale.Crop,
                contentDescription = null,
                colorFilter = if (isLocked) ColorFilter.colorMatrix(
                    ColorMatrix().apply {
                        setToSaturation(0f)
                    }) else null,
                modifier = Modifier.height(200.dp)
            )
            Text(
                text = card.name,
                color = Color.White,
                modifier = Modifier
                    .clip(RoundedCornerShape(bottomEnd = 16.dp))
                    .background(MaterialTheme.colors.primary)
                    .padding(8.dp)
                    .align(Alignment.TopStart)
                    .width(90.dp),
                textAlign = TextAlign.Center,
                fontWeight = FontWeight.SemiBold,
                fontSize = 26.sp,
            )
            Text(
                text = card.description,
                color = Color.DarkGray,
                fontWeight = FontWeight.SemiBold,
                modifier = Modifier
                    .background(MaterialTheme.colors.primary)
                    .fillMaxWidth()
                    .padding(vertical = 8.dp)
                    .padding(start = 4.dp)
                    .align(Alignment.BottomCenter),
                fontSize = 14.sp,
                maxLines = 2,
            )
            if (isLocked) {
                Text(
                    text = "Niveau ${card.levelRequired}",
                    fontWeight = FontWeight.SemiBold,
                    color = Color.White,
                    modifier = Modifier
                        .align(Alignment.TopEnd)
                        .clip(RoundedCornerShape(bottomStart = 12.dp))
                        .background(DeepOrange)
                        .padding(8.dp),
                    fontSize = 18.sp,
                )
            }
        }
    }
}

@Preview
@Composable
private fun ThemeCardPreview() {
    ThemeCard(
        Modifier.size(200.dp), {}, Theme(
            id = 1,
            name = "name",
            description = "description",
            image = URL("https://picsum.photos/200/300"),
            levelRequired = 0
        )
    )
}
