package com.cubifan.vachercher.feature.camera

import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Card
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.viewmodel.compose.viewModel
import com.cubifan.vachercher.ui.theme.Orange

@Composable
fun CustomSnackbar(objectToFind: String, modifier: Modifier = Modifier) {
    Card(
        shape = RoundedCornerShape(bottomStart = 14.dp, bottomEnd = 14.dp),
        backgroundColor = Orange,
        modifier = modifier
            .height(60.dp)
            .fillMaxWidth(.8f)
    ) {
        Column(horizontalAlignment = Alignment.CenterHorizontally) {
            Text(text = "Va chercher ...", fontWeight = FontWeight.Light)
            Text(text = objectToFind, fontSize = 30.sp, fontWeight = FontWeight.SemiBold)
        }
    }
}

@Preview
@Composable
fun PopupPreview(viewModel: CameraViewModel = viewModel()) {
    Box(Modifier.fillMaxSize()) {
        Box(
            contentAlignment = Alignment.TopCenter, modifier = Modifier
                .fillMaxWidth()
                .padding(top = 10.dp)
        ) {
            CustomSnackbar("Agrume")
        }
    }
}