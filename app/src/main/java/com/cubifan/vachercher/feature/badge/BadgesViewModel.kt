package com.cubifan.vachercher.feature.badge

import androidx.compose.runtime.mutableStateListOf
import androidx.lifecycle.ViewModel

class BadgesViewModel : ViewModel() {
    private var levelBadges = 1 to TOTAL_LEVEL_BADGES
    private var scoreBadges = 0 to TOTAL_SCORE_BADGES
    private var winBadges = 0 to TOTAL_WIN_BADGES

    var obtainedBadges = mutableStateListOf(levelBadges, scoreBadges, winBadges)

    companion object {
        const val TOTAL_LEVEL_BADGES = 9
        const val TOTAL_SCORE_BADGES = 9
        const val TOTAL_WIN_BADGES = 9
    }
}