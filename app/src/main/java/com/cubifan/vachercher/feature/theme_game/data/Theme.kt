package com.cubifan.vachercher.feature.theme_game.data

import java.net.URL

data class Theme(
    val id: Int,
    val name: String,
    val description: String,
    val image: URL,
    val levelRequired: Int
)