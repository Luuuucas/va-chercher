export type AmplifyDependentResourcesAttributes = {
    "auth": {
        "vachercher650ea120": {
            "IdentityPoolId": "string",
            "IdentityPoolName": "string",
            "UserPoolId": "string",
            "UserPoolArn": "string",
            "UserPoolName": "string",
            "AppClientIDWeb": "string",
            "AppClientID": "string"
        }
    },
    "storage": {
        "photos": {
            "BucketName": "string",
            "Region": "string"
        }
    }
}